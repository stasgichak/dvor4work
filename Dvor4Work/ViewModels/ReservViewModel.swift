//
//  ReservViewModel.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 02.01.2022.
//

import Foundation
import UIKit

enum ReservCellType: String {
    case title
    case attr
    case from = "Начало аренды:"
    case to = "Окончание аренды:"
    case tariff = "Тариф:"
    case time = "Время:"
    case resident
    case addResident = "Добавить резидента"
    case changePlace = "Сменить место"
    
}

final class ReservViewModel: NSObject {

    var items: [(cellType: ReservCellType, value: [(cellType: ReservCellType, value: Any?)])] = []
    
    init(_ item: ReserveModel) {
        
        if item.type == .office {
            items = [
                        (cellType: .title, value: [(cellType: .title , value: (title: item.name, image: UIImage(named: "temp-Logo")))]),
                        (cellType: .attr, value: [
                                                        (cellType: .from , value: "01.09.2021"),
                                                        (cellType: .to , value: item.toDate),
                                                        (cellType: .tariff , value: "Месяц / ₴14 000")
                                                  ]
                        ),
                        (cellType: .resident, value: [
                                                        (cellType: .resident , value: (name: "Геннадий Литвин", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Арсений Филипчик", phone: "+380785893053"))
                                                  ]
                        ),
                        (cellType: .addResident, value: [ (cellType: .addResident, value: "")])
                    ]
        }
        
        
        if item.type == .soloSpot {
            items = [
                        (cellType: .title, value: [(cellType: .title , value: (title: item.name, image: UIImage(named: "temp-Logo")))]),
                        (cellType: .attr, value: [
                                                        (cellType: .from , value: "01.09.2021"),
                                                        (cellType: .to , value: item.toDate),
                                                        (cellType: .tariff , value: "7 дней / ₴1 300")
                                                  ]
                        ),
                        (cellType: .resident, value: []),
                        (cellType: .changePlace, value: [ (cellType: .changePlace, value: "")])
                    ]
        }
        
        if item.type == .meetingRoom {
            items = [
                        (cellType: .title, value: [(cellType: .title , value: (title: item.name, image: UIImage(named: "temp-Logo")))]),
                        (cellType: .attr, value: [
                                                        (cellType: .to , value: item.toDate),
                                                        (cellType: .time , value: (time: "с " + (item.fromTime ?? "") + " до " + (item.toTime ?? ""), count: "2 часа")),
                                                        (cellType: .tariff , value: "₴350 / час ")
                                                  ]
                        ),
                        (cellType: .resident, value: [
                                                        (cellType: .resident , value: (name: "Геннадий Литвин", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Арсений Филипчик", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "+380645873049", phone: "Отправлен запрос на регистрацию")),
                                                        (cellType: .resident , value: (name: "Арсений Филипчик", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Геннадий Литвин", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Арсений Филипчик", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Геннадий Литвин", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Арсений Филипчик", phone: "+380785893053"))
                                                  ]
                        ),
                        (cellType: .addResident, value: [ (cellType: .addResident, value: "")])
                    ]
        }
        
        if item.type == .webinar {
            items = [
                        (cellType: .title, value: [(cellType: .title , value: (title: item.name, image: UIImage(named: "temp-Logo")))]),
                        (cellType: .attr, value: [
                                                        (cellType: .to , value: item.toDate),
                                                        (cellType: .time , value: "с " + (item.fromTime ?? "") + " до " + (item.toTime ?? "")),
                                                        (cellType: .tariff , value: "₴350 / час ")
                                                  ]
                        ),
                        (cellType: .resident, value: [
                                                        (cellType: .resident , value: (name: "Геннадий Литвин", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "Арсений Филипчик", phone: "+380785893053")),
                                                        (cellType: .resident , value: (name: "+380645873049", phone: "Отправлен запрос на регистрацию"))
                                                  ]
                        ),
                        (cellType: .addResident, value: [ (cellType: .addResident, value: "")])
                    ]
        }
        
    }
    
}
