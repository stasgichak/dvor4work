//
//  ProfileViewModel.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 22.11.2021.
//

import Foundation
import UIKit
final class ProfileViewModel: NSObject {

    var avatar: UIImage?
    var name: String = "Брониогневладислав"
    var family: String = "Константинов"
    static var shared: ProfileViewModel = {
            let instance = ProfileViewModel()
            return instance
        }()

    private override init() {}
        
}
