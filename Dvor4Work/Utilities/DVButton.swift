//
//  DVButton.swift
//  PROTrainer
//
//  Created by Dmitry Vlasenko on 9/18/17.
//  Copyright © 2017 Dmitry Vlasenko. All rights reserved.
//

import UIKit

@IBDesignable class DVButton: UIButton {
    
    @IBInspectable var animatedPress: Bool = false
    @IBInspectable var hideShadowOnPress: Bool = false
    @IBInspectable var scale: CGFloat = 0.0
    
    @IBInspectable var cornerRadius: CGFloat = 0 { // -1 makes it circled
        didSet {
            DispatchQueue.main.async {
                if self.cornerRadius == -1 {
                    self.layer.cornerRadius = self.frame.size.height / 2.0
                } else {
                    self.layer.cornerRadius = self.cornerRadius * self.scale()
                }
            }
        }
    }
    
    //Image
    @IBInspectable var customFrame: Bool = false {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var alignLeft: Bool = false {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var alignCenter: Bool = false {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var alignRight: Bool = false {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var imageX: CGFloat = 0 {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var imageY: CGFloat = 0 {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var widthCustom: CGFloat = 0 {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var heightCustom: CGFloat = 0 {
        didSet {
            setupButton()
        }
    }
    
    // MARK: Border
    var dashedBorder: CAShapeLayer = CAShapeLayer()
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth * self.scale()
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var dashBorder: Bool = false {
        didSet {
           setupButton()
        }
    }
    
    @IBInspectable var dashWidth: CGFloat = 0.0
    @IBInspectable var dashSize: Float = 0.0
    @IBInspectable var dashSpace: Float = 0.0
    
    // MARK: Shadow
    
    @IBInspectable var showShadow: Bool = false
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            guard let color = layer.shadowColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.shadowColor = nil
                return
            }
            
            layer.shadowColor = color.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var maxLines: Int = 1 {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var overlayColor: UIColor? = UIColor.clear {
        didSet {
            setupButton()
        }
    }
    
    @IBInspectable var isPop: Bool = false {
        didSet {
            if isPop {
                addTarget(self, action: #selector(pop), for: .touchUpInside)
            }
        }
    }
    
    @IBInspectable var isDismis: Bool = false {
        didSet {
            if isDismis {
                addTarget(self, action: #selector(dismis), for: .touchUpInside)
            }
        }
    }
    
    @IBInspectable var underLineTitle: Bool = false {
        didSet {
            setupButton()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if animatedPress {
                if isHighlighted {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 5, options: [.curveEaseInOut, .allowUserInteraction], animations: {
                        self.transform = CGAffineTransform.identity.scaledBy(x: self.scale > 0.0 ? self.scale : 0.97, y: self.scale > 0.0 ? self.scale : 0.97)
                    })
                } else {
                    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 5, options: [.curveEaseInOut, .allowUserInteraction], animations: {
                        self.transform = .identity
                    })
                }
                
//                let animation = CABasicAnimation(keyPath: "shadowOpacity")
//                animation.fromValue = 0.0
//                animation.toValue = 1.0
//                animation.duration = 0.5
//                layer.add(animation, forKey: animation.keyPath)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupButton()
        clipsToBounds = false
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupButton()
    }
    
    private func setupButton() {
    
        if dashBorder {
            dashedBorder.removeFromSuperlayer()
            
            dashedBorder.strokeColor = borderColor?.cgColor
            dashedBorder.fillColor = nil
            dashedBorder.lineWidth = dashWidth
            dashedBorder.lineDashPattern = [NSNumber(value: dashSize), NSNumber(value: dashSpace)]
            
            layer.addSublayer(dashedBorder)
            dashedBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            dashedBorder.frame = self.bounds
        }
        
        titleLabel?.numberOfLines = maxLines
        
        if let overlayColor = self.overlayColor, overlayColor != .clear {
            self.imageView?.image = self.imageView?.image?.tintWithColor(color: overlayColor)
        }
        
        if let text = titleLabel?.text, underLineTitle {
            let yourAttributes: [NSAttributedString.Key: Any] = [.underlineStyle: NSUnderlineStyle.single.rawValue]
            
            let attributeString = NSMutableAttributedString(string: text, attributes: yourAttributes)
            self.setAttributedTitle(attributeString, for: .normal)
        }
        
        if customFrame {
            setCustomFrameForButtonImage()
        }
    }
    
    func setCustomFrameForButtonImage() {
        if let imageView = self.imageView, let image = imageView.image {
            var frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
            
            let newHeight = heightCustom == 0 ? frame.height : heightCustom
            let newWidth = widthCustom == 0 ? frame.width : widthCustom
            
            if alignLeft {
                frame = CGRect(x: 0 + self.imageX, y: ((self.height / 2.0) - (newHeight / 2.0)) + imageY, width: newWidth, height: newHeight)
            } else if alignCenter {
                frame = CGRect(x: ((self.width / 2.0) - (newWidth / 2.0) + imageX), y: ((self.height / 2.0) - (newHeight / 2.0)) + imageY, width: newWidth, height: newHeight)
            } else if alignRight {
                frame = CGRect(x: (self.width - newWidth) + self.imageX, y: ((self.height / 2.0) - (newHeight / 2.0) + imageY), width: newWidth, height: newHeight)
            } else {
                frame = CGRect(x: 0 + imageX, y: ((self.height / 2.0) - (newHeight / 2.0) + imageY), width: newWidth, height: newHeight)
            }
            imageView.frame = frame
            
            if overlayColor != .clear {
                self.imageView?.image = self.imageView?.image?.tintWithColor(color: overlayColor!)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupButton()
    }
    
    @objc func pop() {
        parentViewController?.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismis() {
        parentViewController?.dismiss(animated: true, completion: nil)
    }
}
