//
//  UIApplicationExtension.swift
//  KupiSever
//
//  Created by Dmitry Vlasenko on 4/15/19.
//  Copyright © 2019 Maximus Studio. All rights reserved.
//

import UIKit

extension UIApplication {
    
    var topInset: CGFloat {
        return self.safeAreaInsets.top
    }
    
    var bottomInset: CGFloat {
        return self.safeAreaInsets.bottom
    }
    
    var safeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            if let window = UIApplication.shared.keyWindow {
                return window.safeAreaInsets
            } else {
                return .zero
            }
        } else {
            return .zero
        }
    }
    
    var isKeyboardPresented: Bool {
        if let keyboardWindowClass = NSClassFromString("UIRemoteKeyboardWindow"), self.windows.contains(where: { $0.isKind(of: keyboardWindowClass) }) {
            
            UserDefaults.standard.set(false, forKey: "UIRemoteKeyboardWindow")
            return true
        } else {
            return false
        }
    }
    
}
