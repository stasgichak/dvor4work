//
//  SliderView.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 01.12.2021.
//

import Foundation
import UIKit

class SliderView: UIView {

    @IBOutlet private var contentView: UIView!
    @IBOutlet private var actionView: UIView!
    private var _isOn: Bool = false
    var isOn: Bool {
        get { return _isOn }
        set { _isOn = newValue
            if newValue {
                UIView.animate(withDuration: 0.2, animations: {
                    self.actionView.transform = CGAffineTransform(translationX: 18, y: 0)
                    self.alpha = 1.0
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.actionView.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.alpha = 0.3
                })
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
           initSubviews()
       }

       override init(frame: CGRect) {
           super.init(frame: frame)
           initSubviews()
       }

       func initSubviews() {
           // standard initialization logic
           let nib = UINib(nibName: "SliderView", bundle: nil)
           nib.instantiate(withOwner: self, options: nil)
           contentView.frame = bounds
           addSubview(contentView)
           
           contentView.translatesAutoresizingMaskIntoConstraints = false
           contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
           contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
           contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
           contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true

           
           /*let tap = UITapGestureRecognizer(target: self, action: #selector(self.changeState(_:)))
           self.addGestureRecognizer(tap)*/
           self.alpha = 0.3
           
       }
    
    @objc func changeState(_ sender: UITapGestureRecognizer? = nil) {
        isOn = !isOn
    }

}
