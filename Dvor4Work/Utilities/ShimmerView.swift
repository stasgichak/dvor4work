//
//  ShimmerView.swift
//  Chronicle
//
//  Created by Admin on 25.08.2021.
//

import Foundation
import UIKit
class ShimmerView: UIView {

    var gradientColorOne : CGColor = UIColor(white: 0.0, alpha: 0.0).cgColor
    var gradientColorTwo : CGColor = UIColor(white: 1.0, alpha: 0.2).cgColor
    
    
    
    func addGradientLayer() -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.backgroundColor = UIColor.clear.cgColor
        gradientLayer.frame = self.bounds
        print("-------------------------------------------------------")
        print(self.bounds)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 3.0, y: 1.0)
        gradientLayer.colors = [ gradientColorOne, gradientColorTwo, gradientColorOne]
        gradientLayer.locations = [0.54, 0.55, 0.56]
        gradientLayer.name = "Grad"
        self.layer.addSublayer(gradientLayer)
        
        return gradientLayer
    }
    
    func addAnimation() -> CABasicAnimation {
       
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [-0.65, -0.5, -0.35]
        animation.toValue = [1.35, 1.5, 1.65]
        animation.repeatCount = .infinity
        animation.duration = 1.0
        return animation
    }
    
    func startAnimating() {
        stopAnimating()
        var isAdded = false
        for i in self.layer.sublayers! {
            if i.name == "Grad" {
                i.frame = self.bounds
                isAdded = true
            }
        }
        
        
        if !isAdded {
            let gradientLayer = addGradientLayer()
            let animation = addAnimation()
           
            gradientLayer.add(animation, forKey: animation.keyPath)
            
        }
    }
    
    func stopAnimating(){
        for i in self.layer.sublayers! {
            if i.name == "Grad" {
                i.removeFromSuperlayer()
            }
        }
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        for i in self.layer.sublayers! {
            if i.name == "Grad" {
                i.frame = self.bounds
            }
        }
    }
}
