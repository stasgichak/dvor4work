//
//  UIButton+Extensions.swift
//  eShake
//
//  Created by Admin on 25.09.2020.
//  Copyright © 2020 SG. All rights reserved.
//

import Foundation
import UIKit
extension UIButton {
    override open var intrinsicContentSize: CGSize {
        let intrinsicContentSize = super.intrinsicContentSize

        let adjustedWidth = intrinsicContentSize.width + titleEdgeInsets.left + titleEdgeInsets.right
        let adjustedHeight = intrinsicContentSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom

        return CGSize(width: adjustedWidth, height: adjustedHeight)
    }
}
