//
//  tintImage.swift
//  PROTrainer
//
//  Created by MADSOFT on 19/09/2017.
//  Copyright © 2017 Dmitry Vlasenko. All rights reserved.
//

import UIKit

extension String {
    
    func formattedNumber() -> String {
        let cleanPhoneNumber = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "+XXX (XX) XXX-XX-XX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
}

extension Dictionary {
    
    func clearNulls() -> [Key: Any] {
        return filter({ $0.value as? NSNull == nil })
    }
    
}

extension UIImage {
    
    public func tintWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(CGBlendMode.normal)
        let rect: CGRect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context.fill(rect)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 0.5)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func scaleImage(maxCompressRatio maxRatio: CGFloat, size: CGSize) -> Data? {
        
        let imageSize = self.size
        var width = size.width
        var height = size.height
        
        if imageSize.height > imageSize.width {
            let factor = imageSize.height / imageSize.width
            width = size.height / factor
        } else {
            let factor = imageSize.width / imageSize.height
            height = size.width / factor
        }
        
        //Scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 1.0)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let MAX_UPLOAD_SIZE: CGFloat = 0.5
        var compression: CGFloat = 1.0
        let maxCompression: CGFloat = maxRatio
        if var imageData = image!.jpegData(compressionQuality: compression) {
            while CGFloat(imageData.count) > MAX_UPLOAD_SIZE && compression > maxCompression {
                compression -= 0.10
                imageData = image!.jpegData(compressionQuality: compression)!
            }
            return imageData
        } else {
            return nil
        }
        
    }
    
    func scaleImage(_ size: CGSize) -> UIImage? {
        let imageSize = self.size
        var width = size.width
        var height = size.height
        
        if imageSize.height > imageSize.width {
            let factor = imageSize.height / imageSize.width
            width = size.height / factor
        } else {
            let factor = imageSize.width / imageSize.height
            height = size.width / factor
        }
        
        //Scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 1.0)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func imageWithSize(size: CGSize, contentMode: UIView.ContentMode) -> UIImage {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth: CGFloat = size.width / self.size.width
        let aspectHeight: CGFloat = size.height / self.size.height
        let aspectRatio: CGFloat = contentMode == .scaleAspectFit ? min(aspectWidth, aspectHeight) : max(aspectWidth, aspectHeight)
        
        scaledImageRect.size.width = self.size.width * aspectRatio
        scaledImageRect.size.height = self.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        self.draw(in: scaledImageRect)
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    func scaleImage(withWidth width: CGFloat) -> UIImage {
        let imageSize = self.size

        let height = imageSize.height * (width / imageSize.width)
        
        //Scale
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 1.0)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
}
