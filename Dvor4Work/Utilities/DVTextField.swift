//
//  DVTextField.swift
//  PROTrainer
//
//  Created by Dmitry Vlasenko on 9/18/17.
//  Copyright © 2017 Dmitry Vlasenko. All rights reserved.
//

import UIKit

@IBDesignable class DVTextField: UITextField {
    
    var headerLabel: UILabel!
    var bottomLine = CALayer()
    var imageView: UIImageView?
    
    // MARK: Bottom line
    @IBInspectable var addBottomLine: Bool = false {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var showLine: Bool = false {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var lineColor: UIColor = .gray {
        didSet {
            if showLine {
                setupTextField()
            }
        }
    }
    
    @IBInspectable var lineWidth: CGFloat = 0.5 {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = .gray {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var cursorColor: UIColor? = .gray {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            DispatchQueue.main.async {
                if self.cornerRadius == -1 {
                    self.layer.cornerRadius = self.height / 2.0
                } else {
                    self.layer.cornerRadius = self.cornerRadius * self.scale()
                }
            }
        }
    }
    
    // MARK: Border
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    // MARK: Shadow
    
    @IBInspectable var showShadow: Bool = false
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    // MARK: Text inset
    @IBInspectable var textRectRight: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var textRectX: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var textRectY: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var headerString: String? = "" {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var headerLabelColor: UIColor? = .clear {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var headerLabelSize: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    // MARK: Image
    @IBInspectable var image: UIImage? = nil {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var imageX: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var widthImage: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    @IBInspectable var heightImage: CGFloat = 0.0 {
        didSet {
            setupTextField()
        }
    }
    
    // MARK: Style
    @IBInspectable var isPhone: Bool = false {
        didSet {
            if isPhone {
                addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
            }
        }
    }
    
    @IBInspectable var onlyNumbers: Bool = false {
        didSet {
            if onlyNumbers {
                addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
            }
        }
    }
    
    @IBInspectable var removeWhiteSpaces: Bool = false {
        didSet {
            if removeWhiteSpaces {
                addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
            }
        }
    }
    
    @IBInspectable var isUpperCase: Bool = false {
        didSet {
            if isUpperCase {
                addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
            }
        }
    }
    
    @IBInspectable var isLowerCase: Bool = false {
        didSet {
            if isLowerCase {
                addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
            }
        }
    }
    
    @IBInspectable var maxLength: Int = 0 {
        didSet {
            if maxLength > 0 {
                delegate = self
            }
        }
    }
    
    @IBInspectable var canEdit: Bool = true {
        didSet {
            isEnabled = canEdit
        }
    }
    
    override var placeholder: String? {
        didSet {
            super.placeholder = placeholder
            setupTextField()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTextField()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        setupTextField()
    }
    
    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                _ = becomeFirstResponder()
            }
        }
    }

    override func becomeFirstResponder() -> Bool {

        let success = super.becomeFirstResponder()
        if isSecureTextEntry, let text = self.text {
            self.text?.removeAll()
            insertText(text)
        }
         return success
    }
    
    private func setupTextField() {
        if addBottomLine {
            bottomLine.removeFromSuperlayer()
            DispatchQueue.main.async {
                self.bottomLine.backgroundColor = self.lineColor.cgColor
                self.bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - self.lineWidth, width: self.frame.size.width, height: self.lineWidth)
                self.layer.addSublayer(self.bottomLine)
            }
        }
        
        //placeholder settings
        let attributes = [
            NSAttributedString.Key.foregroundColor: placeholderColor,
            NSAttributedString.Key.font: self.font
        ] 
        
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: attributes as [NSAttributedString.Key: Any])
        
        //cursorColor
        UITextField.appearance().tintColor = cursorColor ?? textColor
        
        imageView?.removeFromSuperview()
        imageView = UIImageView(frame: CGRect(x: imageX, y: ((CGFloat(self.frame.height) / 2.0) - (heightImage / 2.0)), width: widthImage, height: heightImage))
        imageView?.contentMode = .scaleAspectFit
        imageView?.image = image
        addSubview(imageView!)
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setupTextField()
//    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + (textRectX * scale()), y: bounds.origin.y + (textRectY * scale()), width: bounds.width - ((textRectX * scale()) + (textRectRight * scale())), height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + (textRectX * scale()), y: bounds.origin.y + (textRectY * scale()), width: bounds.width - ((textRectX * scale()) + (textRectRight * scale())), height: bounds.height)
    }
    
    @objc func textDidChanged() {
        
        var allowedСharacters: [String] = []
        if onlyNumbers {
            allowedСharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        } else if isPhone {
            allowedСharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+"]
        }
        
        if isPhone || onlyNumbers {
            let textArray = Array(self.text!)
            for character in textArray where !allowedСharacters.contains("\(character)") {
                self.text = self.text?.replacingOccurrences(of: "\(character)", with: "")
            }
        }
        
        if removeWhiteSpaces {
            text = text!.trimmingCharacters(in: .whitespaces)
        }
        
        if isUpperCase {
            text = text?.uppercased()
        }
        
        if isLowerCase {
            text = text?.lowercased()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension DVTextField: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return canEdit
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
