//
//  DVView.swift
//  PROTrainer
//
//  Created by MADSOFT on 21/09/2017.
//  Copyright © 2017 Dmitry Vlasenko. All rights reserved.
//

import UIKit

@IBDesignable class DVView: UIView, UIGestureRecognizerDelegate {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 { // -1 makes it circled
        didSet {
            DispatchQueue.main.async {
                if self.cornerRadius == -1 {
                    self.layer.cornerRadius = (self.frame.size.height * self.scale()) / 2.0
                } else {
                    self.layer.cornerRadius = self.cornerRadius * self.scale()
                }
            }
        }
    }
    
    // MARK: Border
    @IBInspectable var showBorder: Bool = false
    var dashedBorder: CAShapeLayer = CAShapeLayer()
    let line = CAShapeLayer()
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var dashBorder: Bool = false {
        didSet {
            setupView()
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0.0
    @IBInspectable var dashSize: Float = 0.0
    @IBInspectable var dashSpace: Float = 0.0
    
    @IBInspectable var dashedView: Bool = false {
        didSet {
            setupView()
        }
    }
    
    // MARK: Shadow
    
    @IBInspectable var showShadow: Bool = false
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var hideKeyBoardOnTap: Bool = false {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var swipeToPop: Bool = false {
        didSet {
            setupView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        clipsToBounds = false
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        setupView()
    }
    
    private func setupView() {
        if dashBorder {
            dashedBorder.removeFromSuperlayer()
            
            dashedBorder.strokeColor = self.borderColor?.cgColor
            dashedBorder.fillColor = nil
            dashedBorder.lineWidth = self.dashWidth
            dashedBorder.lineDashPattern = [NSNumber(value: self.dashSize), NSNumber(value: self.dashSpace)]
            
            layer.addSublayer(dashedBorder)
            dashedBorder.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
            dashedBorder.frame = self.bounds
        }
        
        if hideKeyBoardOnTap {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap))
            tapGesture.delegate = self
            addGestureRecognizer(tapGesture)
        }
        
        if dashedView {
            line.removeFromSuperlayer()
            let linePath = UIBezierPath()
            linePath.move(to: CGPoint(x: 0, y: 0))
            linePath.addLine(to: CGPoint(x: width, y: 0))
            line.path = linePath.cgPath
            line.strokeColor = borderColor?.cgColor
            line.lineWidth = height
            line.lineJoin = CAShapeLayerLineJoin.round
            line.lineDashPattern = [NSNumber(value: dashSize), NSNumber(value: dashSpace)]
            self.layer.addSublayer(line)
        }
        
        if swipeToPop {
            parentViewController?.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            parentViewController?.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return swipeToPop ? true : false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return UIApplication.shared.isKeyboardPresented
    }
    
    @objc func tap() {
        endEditing(true)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupView()
    }
    
}
