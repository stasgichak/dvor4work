//
//  UIViewExtensions.swift
//  DVControls
//
//  Created by Dmitry Vlasenko on 10/1/17.
//  Copyright © 2017 Dmitry Vlasenko. All rights reserved.
//

import UIKit

extension UIView {
    
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            frame.size.width = newValue
        }
    }
    
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            frame.size.height = newValue
        }
    }
    
    var x: CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            frame.origin.x = newValue
        }
    }
    
    var y: CGFloat {
        get {
            return frame.origin.y
        }
        set {
            frame.origin.y = newValue
        }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func addBottomLine() {
        let bottomLine = CALayer()
        bottomLine.borderColor = UIColor(hex: 0xc2c2cf)?.cgColor
        bottomLine.frame = CGRect(x: 0, y: frame.size.height - 1, width: frame.size.width, height: 1.0)
        bottomLine.borderWidth = 1.0
        layer.addSublayer(bottomLine)
    }
    
    func animateBorderWidth(toValue: CGFloat, duration: Double) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "borderWidth")
        animation.fromValue = layer.borderWidth
        animation.toValue = toValue
        animation.duration = duration
        layer.add(animation, forKey: "Width")
        layer.borderWidth = toValue
    }
    
    func hideSubViews(animated: Bool = false) {
        UIView.animate(withDuration: animated ? 0.3 : 0) {
            for view in self.subviews {
                view.alpha = 0
            }
        }
    }
    
    func showSubViews(animated: Bool = false) {
        UIView.animate(withDuration: animated ? 0.3 : 0) {
            for view in self.subviews {
                view.alpha = 1
            }
        }
    }
    
    func circleCornerRadius() {
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.height / 2.0
        }
    }
    
    // In order to create computed properties for extensions, we need a key to
    // store and access the stored property
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    // Set our computed property type to a closure
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    // This is the meat of the sauce, here we create the tap gesture recognizer and
    // store the closure the user passed to us in the associated object we declared above
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // Every time the user taps on the UIImageView, this function gets called,
    // which triggers the closure we stored
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    
    func animatePress(increase: Bool) {
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1.5, initialSpringVelocity: 0, options: [.allowUserInteraction, .curveEaseInOut], animations: {
            self.transform = CGAffineTransform(scaleX: increase ? 1.2 : 0.8, y: increase ? 1.2 : 0.8)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1.5, initialSpringVelocity: 0, options: [.allowUserInteraction, .curveEaseInOut], animations: {
                    self.transform = .identity
                }, completion: nil)
            })
        })
    }
    
    func addBlurToView() {
        var blurEffect: UIBlurEffect!
        blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectedView = UIVisualEffectView(effect: blurEffect)
        blurredEffectedView.frame = self.bounds
        blurredEffectedView.alpha = 0.95
        blurredEffectedView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurredEffectedView)
    }
    
    func removeBlurFromView() {
        for subview in self.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat, borderWidth:CGFloat, borderColor:CGColor) {
        
        for i in layer.sublayers! {
            if i is CAShapeLayer {
                i.removeFromSuperlayer()
                
            }
        }
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
        mask.frame = self.bounds
            mask.path = path.cgPath
            layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path// Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
                borderLayer.strokeColor = borderColor
                borderLayer.lineWidth = borderWidth
                borderLayer.frame = self.bounds
            layer.addSublayer(borderLayer)
        
        }

}
