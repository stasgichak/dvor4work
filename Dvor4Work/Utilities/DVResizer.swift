//
//  DVResizer.swift
//
//  Created by Dmitry Vlasenko on 1/25/18.
//  Copyright © 2018 Dmitry Vlasenko. All rights reserved.
//

import UIKit

public enum Size: Int {
    case unknownSize = 0
    case is3_5Inch
    case is4Inch
    case is4_7Inch
    case is5_5Inch
    case is5_8Inch
    case is6_1_6_5
}

public enum Ratio: Int {
    case unknownRation = 0
    case is4_3
    case is16_9
    case is18_9
}

public enum Device: Int {
    case unknown = 0
    case iPhone
    case iPad
}

final class DVResizer: NSObject {
    
    static var baseWidth: CGFloat = 375

    static var scale: CGFloat {
       /* if type == .iPad {
            if UIScreen.main.bounds.size.width == 768 {
                return 1.2
            } else {
                return UIScreen.main.bounds.size.width / 768
            }
        } else {
            return UIScreen.main.bounds.size.width / DVResizer.baseWidth
        }*/
        return 1.0
    }
    
    static var screenHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        return max(width, height)
    }
    
    static var size: Size {
        switch DVResizer.screenHeight {
        case 480:
            return .is3_5Inch
        case 568:
            return .is4Inch
        case 667:
            return UIScreen.main.scale == 3.0 ? .is5_5Inch : .is4_7Inch
        case 736:
            return .is5_5Inch
        case 812:
            return .is5_8Inch
        case 896:
            return .is6_1_6_5
        default:
            return .unknownSize
        }
    }
    
    static var type: Device {
        switch DVResizer.screenHeight {
        case 480, 568, 667, 736, 812, 896:
            return .iPhone
        case 1024, 1112, 1366, 1194:
            return .iPad
        default:
            return .unknown
        }
    }
    
    static var ratio: Ratio {
        switch DVResizer.screenHeight {
        case 480, 1024, 1112, 1366:
            return .is4_3
        case 568, 667, 736:
            return .is16_9
        case 812, 896:
            return .is18_9
        default:
            return .unknownRation
        }
    }

}

protocol PropertyStoring {
    
    associatedtype Constraint
    
    func getAssociatedObject(_ key: UnsafeRawPointer!, defaultValue: Constraint) -> Constraint
}

extension PropertyStoring {
    
    func getAssociatedObject(_ key: UnsafeRawPointer!, defaultValue: Constraint) -> Constraint {
        guard let value = objc_getAssociatedObject(self, key) as? Constraint else {
            return defaultValue
        }
        return value
    }
    
}

extension NSLayoutConstraint: PropertyStoring {
    
    typealias Constraint = CGFloat
    
    private struct CustomProperties {
        static var is5_8 = -1
        static var is3_5 = -1
        static var ignore = 0
    }
    
    @IBInspectable var is5_8: CGFloat {
        get {
            return getAssociatedObject(&CustomProperties.is5_8, defaultValue: NSLayoutConstraint.Constraint(CustomProperties.is5_8))
        }
        set {
            return objc_setAssociatedObject(self, &CustomProperties.is5_8, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    @IBInspectable var is3_5: CGFloat {
        get {
            return getAssociatedObject(&CustomProperties.is3_5, defaultValue: NSLayoutConstraint.Constraint(CustomProperties.is3_5))
        }
        set {
            return objc_setAssociatedObject(self, &CustomProperties.is3_5, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    @IBInspectable var ignore: CGFloat {
        get {
            return getAssociatedObject(&CustomProperties.ignore, defaultValue: NSLayoutConstraint.Constraint(CustomProperties.ignore))
        }
        set {
            return objc_setAssociatedObject(self, &CustomProperties.ignore, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if is5_8 != -1 && DVResizer.ratio == .is18_9 {
            constant = DVResizer.size == .is5_8Inch ? is5_8 : is5_8 * (896 / 812)
        } else if is3_5 != -1 && DVResizer.ratio == .is4_3 {
            constant = is3_5
        } else {
            if ignore == 0 {
                constant *= DVResizer.scale
            }
        }
    }
    
    func setMultiplier(multiplier: CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        if let firstItem = firstItem {
            let newConstraint = NSLayoutConstraint(
                item: firstItem,
                attribute: firstAttribute,
                relatedBy: relation,
                toItem: secondItem,
                attribute: secondAttribute,
                multiplier: multiplier,
                constant: constant)
            
            newConstraint.priority = priority
            newConstraint.shouldBeArchived = shouldBeArchived
            newConstraint.identifier = identifier
            
            NSLayoutConstraint.activate([newConstraint])
            return newConstraint
        } else {
            return self
        }
    }
    
}

extension UILabel {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        font = font.withSize(font.pointSize * DVResizer.scale)
    }
    
}

extension UIButton {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if let titleLabel = titleLabel {
            titleLabel.font = titleLabel.font.withSize(titleLabel.font.pointSize * DVResizer.scale)
        }
    }
    
}

extension UITextField {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if let font = font {
            self.font = font.withSize(font.pointSize * DVResizer.scale)
        }
    }
    
}

extension UITextView {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if let font = font {
            self.font = self.font?.withSize(font.pointSize * DVResizer.scale)
        }
    }
    
}

extension UIStackView {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        spacing *= DVResizer.scale
    }
    
}

extension UIFont {
    
    func resize() -> UIFont {
        return withSize(pointSize * DVResizer.scale)
    }
    
}

extension NSObject {
    
    func scale() -> CGFloat {
        return DVResizer.scale
    }
    
}
