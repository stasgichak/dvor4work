//
//  ShimmerLabel.swift
//  Chronicle
//
//  Created by Admin on 26.08.2021.
//

import Foundation
import UIKit
class ShimmerLabel: UILabel {

    var gradientColorOne : CGColor = UIColor(white: 0.0, alpha: 0.0).cgColor
    var gradientColorTwo : CGColor = UIColor(white: 1.0, alpha: 0.4).cgColor
    
    
    
    func addGradientLayer() -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.backgroundColor = UIColor.clear.cgColor
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 3.0, y: 1.0)
        gradientLayer.colors = [ gradientColorOne, gradientColorTwo, gradientColorOne]
        gradientLayer.locations = [0.54, 0.55, 0.56]
        gradientLayer.name = "Grad"
        //self.layer.addSublayer(gradientLayer)
        self.layer.mask = gradientLayer
        return gradientLayer
    }
    
    func addAnimation() -> CABasicAnimation {
       
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [-0.6, -0.5, -0.4]
        animation.toValue = [1.4, 1.5, 1.6]
        animation.repeatCount = .infinity
        animation.duration = 1.0
        return animation
    }
    
    func startAnimating() {
        
        var isAdded = false
        if let l = self.layer.sublayers {
            for i in l {
                if i.name == "Grad" {
                    i.frame = self.bounds
                    isAdded = true
                }
            }
        }
        
        
        if !isAdded {
            let gradientLayer = addGradientLayer()
            let animation = addAnimation()
           
            gradientLayer.add(animation, forKey: animation.keyPath)
        }
    }
    
    func stopAnimating(){
        for i in self.layer.sublayers! {
            if i.name == "Grad" {
                i.removeFromSuperlayer()
            }
        }
    }
}
