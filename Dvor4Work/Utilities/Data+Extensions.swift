//
//  Data+Extensions.swift
//  eShake
//
//  Created by Admin on 02.09.2020.
//  Copyright © 2020 SG. All rights reserved.
//

import Foundation
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
