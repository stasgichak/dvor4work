//
//  TimerView.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 21.01.2022.
//

import Foundation
import UIKit

class TimerView: UIView {
    let kCONTENT_XIB_NAME = "TimerView"
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var timingView: UIView!
    var labels: [UILabel] = []
    var isOpen: [Bool] = []
    var periodView: UIView?
    var leftView: UIView?
    var rightView: UIView?
    var left = 0
    var right = 3
    var delegate: TimerViewDelegate?
    var isCorrectPeriod = false
    var date:Date?
    @IBOutlet weak var scrollView: UIScrollView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        date = Date()
        periodView = UIView()
        periodView?.backgroundColor = UIColor(hexString: "#595D51", alpha: 0.5)
        periodView?.frame = CGRect(x: 0, y: 1, width: 100, height: 44)
        self.timingView.addSubview(periodView ?? UIView())
        
        leftView = UIView()
        leftView?.backgroundColor = .clear
        leftView?.frame = CGRect(x: CGFloat(left * 25) - 12.5, y: 1, width: 25, height: 44)
        self.timingView.addSubview(leftView ?? UIView())
        
        let l = UIView()
        l.backgroundColor = UIColor(named: "bgColor")
        l.layer.cornerRadius = 7.5
        l.layer.borderWidth = 1
        l.layer.borderColor = UIColor(hexString: "#10201D")?.cgColor
        l.frame = CGRect(x: 5, y: 16, width: 15, height: 15)
        self.leftView?.addSubview(l)
        
        rightView = UIView()
        rightView?.backgroundColor = .clear
        rightView?.frame = CGRect(x: CGFloat((right + 1) * 25) - 12.5, y: 1, width: 25, height: 44)
        self.timingView.addSubview(rightView ?? UIView())
        
        let r = UIView()
        r.backgroundColor = UIColor(named: "bgColor")
        r.layer.cornerRadius = 7.5
        r.layer.borderWidth = 1
        r.layer.borderColor = UIColor(hexString: "#10201D")?.cgColor
        r.frame = CGRect(x: 5, y: 16, width: 15, height: 15)
        self.rightView?.addSubview(r)
        
        leftView?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.leftPanGesture(_:))))
        rightView?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.rightPanGesture(_:))))
        
        drawViews()
    }
    
    func drawViews(){
        for _ in 0..<97 {
            isOpen.append(true)
        }
        closeCurrentDay()
        var openTimes = 0
        var isFoundTime = false
        for i in 0..<97 {
            if i % 2 == 0 {
                let v = UIView()
                v.backgroundColor = UIColor(hexString: "#10201D",alpha: 0.3)
                v.frame = CGRect(x: i * 25, y: 12, width: 1, height: 33)
                self.timingView.addSubview(v)
                let l = UILabel()
                let h = i / 4
                let m = (i % 4) * 15
                l.text =  ((h < 10) ? "0" : "") + String(h) + ":" + ((m < 10) ? "0" : "") + String(m)
                l.textColor = UIColor(named: "dark")
                l.textAlignment = .center
                l.frame = CGRect(x: (i - 1) * 25, y: 0, width: 50, height: 14)
                l.font = UIFont(name: "MazzardM-Regular", size: 10)
                if i == 0 || i == 96 {
                    l.alpha = 0
                }
                self.mainView.addSubview(l)
                labels.append(l)
            } else {
                let v = UIView()
                v.backgroundColor = UIColor(hexString: "#10201D",alpha: 0.3)
                v.frame = CGRect(x: i * 25 - 1, y: 29, width: 1, height: 16)
                self.timingView.addSubview(v)
            }
            
           
            
            
        }
        updateImage()
        
        for i in 0..<97 {
            let v = UIView()
            v.backgroundColor = .clear
            v.tag = i
            v.frame = CGRect(x: i * 25, y: 0, width: 25, height: 46)
            self.timingView.addSubview(v)
            v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapViewAction(_:))))
        }
        
        timingView.bringSubviewToFront(leftView!)
        timingView.bringSubviewToFront(rightView!)
        
        
    }
    
    @objc func tapViewAction(_ sender: UITapGestureRecognizer? = nil) {
        if let t = sender?.view?.tag {
            left = t
            right = t + 3
            validatePeriod()
            delegate?.startTimerViewChanged(left)
            delegate?.endTimerViewChanged(right+1)
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                self.periodView?.frame = CGRect(x: CGFloat((t) * 25),  y: 1, width: 100, height: 44)
                self.leftView?.frame = CGRect(x: CGFloat(self.left * 25) - 12.5, y: 1, width: 25, height: 44)
                self.rightView?.frame = CGRect(x: CGFloat((self.right + 1) * 25) - 12.5, y: 1, width: 25, height: 44)
                //self.periodView?.transform = CGAffineTransform(translationX: CGFloat((t) * 25), y: 0)
            }, completion: nil)
        }
    }
    
    @objc func leftPanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        let p = gestureRecognizer.location(in: timingView)
        let t = Int(round(p.x / 25))
        if left != t {
            if (right - t + 1) >= 4 {
                left = t
                self.periodView?.frame = CGRect(x: ((left) * 25),  y: 1, width: (right - left + 1) * 25, height: 44)
                leftView?.frame = CGRect(x: CGFloat(left * 25) - 12.5, y: 1, width: 25, height: 44)
                validatePeriod()
                delegate?.startTimerViewChanged(left)
            }
        }
    }
    
    @objc func rightPanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        let p = gestureRecognizer.location(in: timingView)
        let t = Int(round(p.x / 25)) - 1
        if t != right {
            if (t - left + 1) >= 4 {
                right = t
                self.periodView?.frame = CGRect(x: ((left) * 25),  y: 1, width: (right - left + 1) * 25, height: 44)
                rightView?.frame = CGRect(x: CGFloat((right + 1) * 25) - 12.5, y: 1, width: 25, height: 44)
                validatePeriod()
                delegate?.endTimerViewChanged(right+1)
            }
        }
    }
    
    
    func validatePeriod() {
        var b = true
        if left>right {
            return
        }
        for i in left...right {
            if !isOpen[i] {
                b = false
            }
        }
        isCorrectPeriod = b
        if !b {
            UIView.animate(withDuration: 0.2, animations: {
                self.periodView?.backgroundColor = UIColor(hexString: "#C26356", alpha: 0.5)
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.periodView?.backgroundColor = UIColor(hexString: "#595D51", alpha: 0.5)
            })
        }
        delegate?.changeStatePeriod(isCorrectPeriod)
    }
    
    func setPeriod(_ start: Int, end: Int){
        
        left = start
        right = end
        self.periodView?.frame = CGRect(x: ((left) * 25),  y: 1, width: (right - left + 1) * 25, height: 44)
        leftView?.frame = CGRect(x: CGFloat(left * 25) - 12.5, y: 1, width: 25, height: 44)
        rightView?.frame = CGRect(x: CGFloat((right + 1) * 25) - 12.5, y: 1, width: 25, height: 44)
        validatePeriod()
        
        
        
        
        var x = CGFloat((left) * 25) - (self.width - CGFloat((right - left + 1) * 25)) / 2
        
        if CGFloat((right - left + 1) * 25) > self.width {
            x = CGFloat((left) * 25) - 25
        }
        
        if x > scrollView.contentSize.width - self.width {
            x = scrollView.contentSize.width - self.width
        }
        
        if x >= 0 {
            self.scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
        } else {
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    
    func closeCurrentDay(){
        for i in 0..<97 {
            isOpen[i] = false
        }
        isOpen[10] = false
        isOpen[11] = false
        isOpen[12] = false
        isOpen[13] = false
        isOpen[14] = false
        isOpen[15] = false
        isOpen[16] = false
        isOpen[32] = false
        isOpen[33] = false
        isOpen[34] = false
        isOpen[35] = false
        isOpen[36] = false
        isOpen[75] = false
        isOpen[76] = false
        isOpen[77] = false
        isOpen[78] = false
        isOpen[81] = false
        isOpen[82] = false
        isOpen[83] = false
        isOpen[84] = false
        isOpen[85] = false
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.YYYY"
        if formatter.string(from: self.date ?? Date()) == formatter.string(from: Date()){
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date ?? Date())
            let minute = calendar.component(.minute, from: date ?? Date())+1
            let c = hour * 4 + Int(ceil(Double(minute)/15))
            for i in 0..<c{
                isOpen[i] = false
            }
        }
        
    }
    
    func updateImage(){
        for i in self.timingView.subviews {
            if let image = i as? UIImageView {
                i.removeFromSuperview()
            }
        }
        var isFoundTime = false
        var openTimes = 0
        for i in 0..<97 {
            if !isOpen[i] {
                openTimes = 0
                let image = UIImageView()
                image.backgroundColor = UIColor(named: "dark")?.withAlphaComponent(0.1)
                image.frame = CGRect(x: i * 25, y: 1, width: 25, height: 44)
                self.timingView.addSubview(image)
            } else {
                if !isFoundTime {
                    openTimes = openTimes + 1
                    if openTimes >= 4 {
                        self.periodView?.frame = CGRect(x: CGFloat((i - 3) * 25),  y: 1, width: 100, height: 44)
                        left = i - 3
                        right = i
                        leftView?.frame = CGRect(x: CGFloat(left * 25) - 12.5, y: 1, width: 25, height: 44)
                        rightView?.frame = CGRect(x: CGFloat((right + 1) * 25) - 12.5, y: 1, width: 25, height: 44)
                        
                        var x = CGFloat((left) * 25) - (self.width - CGFloat((right - left + 1) * 25)) / 2
                        
                        if CGFloat((right - left + 1) * 25) > self.width {
                            x = CGFloat((left) * 25) - 25
                        }
                        
                        if x >= 0 {
                            self.scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
                        } else {
                            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        }
                        delegate?.startTimerViewChanged(left)
                        delegate?.endTimerViewChanged(right+1)
                        isFoundTime = true
                        
                        
                    }
                }
            }
        }
        if !isFoundTime {
            self.periodView?.frame = CGRect(x: 0,  y: 1, width: 100, height: 44)
            left = 0
            right = 3
            leftView?.frame = CGRect(x: CGFloat(left * 25) - 12.5, y: 1, width: 25, height: 44)
            rightView?.frame = CGRect(x: CGFloat((right + 1) * 25) - 12.5, y: 1, width: 25, height: 44)
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
            delegate?.startTimerViewChanged(left)
            delegate?.endTimerViewChanged(right+1)
        }
        validatePeriod()
        timingView.bringSubviewToFront(leftView!)
        timingView.bringSubviewToFront(rightView!)
    }
    
    func setDate(_ date:Date){
        self.date = date
        closeCurrentDay()
        updateImage()
    }
    
}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}

protocol TimerViewDelegate {
    func startTimerViewChanged(_ start:Int)
    func endTimerViewChanged(_ end: Int)
    func changeStatePeriod(_ state: Bool)
}
