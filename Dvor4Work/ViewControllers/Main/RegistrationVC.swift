//
//  RegistrationVC.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 09.11.2021.
//

import Foundation
import UIKit
class RegistrationVC: UIViewController {
    override func viewDidLoad() {
        titleLabel.attributedText = NSMutableAttributedString(string: "Регистрация", attributes: [NSAttributedString.Key.kern: -0.24])
        let paragraphStyle = NSMutableParagraphStyle()

        paragraphStyle.lineHeightMultiple = 1.27
        textLabel.attributedText = NSMutableAttributedString(string: "Для регистрации в системе D4W мы используем BankID. Пожалуйста выберите наиболее удобный для Вас способ авторизации.", attributes: [NSAttributedString.Key.kern: -0.24, NSAttributedString.Key.paragraphStyle: paragraphStyle])
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func signupAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Tabbar", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "MainTabbarController") as? MainTabbarController {
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
    }
}
