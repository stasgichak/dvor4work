//
//  AddMoneyViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 26.12.2021.
//

import Foundation
import UIKit
class AddMoneyViewController: MyViewController {

    override func viewDidLoad() {
        
        addDeposit()
        addNavBar("Пополнение депозита")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.cardChoseAction(_:)))
        self.cardView.addGestureRecognizer(tap)
    }
    
    @IBOutlet weak var doneButton: DVButton!
    @IBOutlet weak var cardView: UIView!
    
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
       if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
           self.doneButton.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height)
       }
    }

    @objc func keyboardWillDisappear(_ notification: NSNotification) {
       if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.doneButton.transform = CGAffineTransform(translationX: 0, y: 0)
       }
   }
    @IBAction func doneAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func cardChoseAction(_ sender: UITapGestureRecognizer? = nil) {
        let storyboard = UIStoryboard(name: "Deposit", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "CardSelectionViewController") as? CardSelectionViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

