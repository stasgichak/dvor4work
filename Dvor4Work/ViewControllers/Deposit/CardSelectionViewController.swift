//
//  CardSelectionViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 27.12.2021.
//

import Foundation
import UIKit
class CardSelectionViewController: MyViewController, UITableViewDelegate, UITableViewDataSource, UpdateTableView {
    func update() {
        tableView.reloadData()
    }
    
    var items: [CardModel] = []
    @IBOutlet weak var tableView: UITableView!
    var selectedCard = 0

    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        addDeposit()
        addNavBar("Выбор карты")
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        
        items = [
                    CardModel("ПриватБанк", number:"12**67", date: "01/24", isActive: true),
                    CardModel("Райффайзен банк Аваль", number:" 34**89", date: "04/27", isActive: true),
                    CardModel("ПриватБанк", number: "34**89", date: "05/28", isActive: true),
                    CardModel("УкрСиббанк", number: "67**89", date: "07/30", isActive: true)
                ]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return items.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add", for: indexPath)
            cellBackView(cell)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "card", for: indexPath) as? CardCell else { return UITableViewCell() }
            cell.titleLabel.text = items[indexPath.row].name
            cell.dateLabel.text = items[indexPath.row].date
            cell.numberLabel.text = items[indexPath.row].number
            if items[indexPath.row].isActive {
                cell.titleLabel.alpha = 1.0
                cell.numberLabel.alpha = 1.0
            } else {
                cell.titleLabel.alpha = 0.3
                cell.numberLabel.alpha = 0.3
            }
            
            if selectedCard == indexPath.row {
                cell.supportView?.image = UIImage(named: "check-on")
            } else {
                cell.supportView?.image = UIImage(named: "check-off")
            }
            cellBackView(cell)
            return cell
        }
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "AddCardViewController") as? AddCardViewController {
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            selectedCard = indexPath.row
            self.tableView.reloadData()
        }
    }
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func doneAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

