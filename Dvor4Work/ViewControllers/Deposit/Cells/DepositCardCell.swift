//
//  DepositCardCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 26.12.2021.
//

import Foundation
import UIKit

class DepositCardCell: UITableViewCell {
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
}
