//
//  DepositBalanceCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 26.12.2021.
//

import Foundation
import UIKit
class DepositBalanceCell: UITableViewCell {
    
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
}
