//
//  DepositViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 19.12.2021.
//

import Foundation
import UIKit
class DepositViewController: MyViewController, UITableViewDelegate, UITableViewDataSource, UpdateTableView {
    func update() {
        tableView.reloadData()
    }
    
    var items: [DepositModel] = []
    @IBOutlet weak var tableView: UITableView!
    

    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        addDeposit()
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        items = [
                    DepositModel("Пополнение с карты ПриватБанка", number:"12**67", date: "01.09.21", time: "21:00", price: 2000),
                    DepositModel("Пополнение с карты Монобанка", number: "12**67", date: "29.08.21", time: "20:00", price: 500),
                    DepositModel("Пополнение с карты УкрСиббанка", number: "12**67", date: "20.08.21", time: "08:00", price: 9999),
                    DepositModel("Пополнение с карты Райффайзен банка Аваль", number:" 34**89", date: "12.08.21", time: "11:32", price: 10000),
                    DepositModel("Пополнение с карты ПриватБанка", number: "34**89", date: "01.09.21", time: "21:00", price: 30000),
                    DepositModel("Пополнение с карты Креди Агриколь Банка", number: "12**67", date: "29.08.21", time: "20:00", price: 100000),
                    DepositModel("Пополнение с карты УкрСиббанка", number: "67**89", date: "01.09.21", time: "21:00", price: 9999999),
                    DepositModel("Пополнение с карты Креди Агриколь Банка", number: "34**89", date: "29.08.21", time: "20:00", price: 1),
                    DepositModel("Пополнение с карты Монобанка", number: "56**00", date: "20.08.21", time: "08:00", price: 10),
                    DepositModel("Пополнение с карты Райффайзен банка Аваль", number: "56**00", date: "12.08.21", time: "11:32", price: 13000),
                    DepositModel("Пополнение с карты ПриватБанка", number: "56**00", date: "01.09.21", time: "21:00", price: 1300),
                    DepositModel("Пополнение с карты Креди Агриколь Банка", number: "56**00", date: "29.08.21", time: "20:00", price: 5)
                ]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "balance", for: indexPath) as? DepositBalanceCell else { return UITableViewCell() }
                cell.balanceLabel.text = "₴"+Double(12000).formattedWithSeparatorDecimal
                cellBackView(cell)
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "addMoney", for: indexPath)
                cellBackView(cell)
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as? ProfileTitle else { return UITableViewCell() }
                cellBackView(cell)
                return cell
            }
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cards", for: indexPath) as? DepositCardCell else { return UITableViewCell() }
            cell.titleView.text = items[indexPath.row].name + "\n" + items[indexPath.row].number
            cell.priceLabel.text = "₴ "+items[indexPath.row].price.formattedWithSeparatorDecimal
            var str = NSMutableAttributedString(string: items[indexPath.row].date + " ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "C26356")])
            str.append(NSMutableAttributedString(string: items[indexPath.row].time, attributes: [:]))
            cell.dateLabel.attributedText = str
            cellBackView(cell)
            return cell
        }
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Deposit", bundle: nil)
        if indexPath.section == 0 {
            if indexPath.row == 1 {
                if let vc = storyboard.instantiateViewController(withIdentifier: "AddMoneyViewController") as? AddMoneyViewController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
}

