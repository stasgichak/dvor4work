//
//  MyViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 16.11.2021.
//

import Foundation
import UIKit
class MyViewController: UIViewController {
    
    var depositLabel: UILabel?
    var notificationButton: UIButton?
    var notificationCount: UILabel?
    var depositView: UIView?
    var backButton: UIButton?
    
    var navbar: UIView?
    func addDeposit(){
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let window = windowScene?.windows.first
        self.navigationItem.hidesBackButton = true
        
        let dv = UIView()
        
        self.view.addSubview(dv)
        dv.translatesAutoresizingMaskIntoConstraints = false
        dv.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -1).isActive = true
        dv.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 1).isActive = true
        dv.topAnchor.constraint(equalTo: self.view.topAnchor,constant: (window?.safeAreaInsets.top ?? 0)).isActive = true
        dv.heightAnchor.constraint(equalToConstant: 42).isActive = true
        dv.layer.borderColor = UIColor(named: "dark")?.cgColor
        dv.layer.borderWidth = 1
        dv.backgroundColor = .clear
        
        
        var configuration = UIButton.Configuration.filled()
        configuration.title = ""
        configuration.image = UIImage(named: "Union")
        configuration.titlePadding = 10
        configuration.imagePadding = 10
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)
        configuration.baseBackgroundColor = .clear
        
        notificationButton = UIButton(configuration: configuration, primaryAction: nil)
        dv.addSubview((notificationButton)!)
        notificationButton?.translatesAutoresizingMaskIntoConstraints = false
        notificationButton?.bottomAnchor.constraint(equalTo: dv.bottomAnchor).isActive = true
        notificationButton?.trailingAnchor.constraint(equalTo: dv.trailingAnchor).isActive = true
        notificationButton?.topAnchor.constraint(equalTo: dv.topAnchor).isActive = true
        notificationButton?.widthAnchor.constraint(equalToConstant: 70).isActive = true
        notificationButton?.layer.borderColor = UIColor(named: "dark")?.cgColor
        notificationButton?.layer.borderWidth = 1
        notificationButton?.backgroundColor = .clear
        
        notificationCount = PaddingLabel()
        dv.addSubview((notificationCount)!)
        notificationCount?.text = "555"
        notificationCount?.translatesAutoresizingMaskIntoConstraints = false
        notificationCount?.topAnchor.constraint(equalTo: dv.topAnchor , constant: 4).isActive = true
        notificationCount?.leftAnchor.constraint(equalTo: notificationButton!.centerXAnchor).isActive = true
        notificationCount?.layer.borderColor = UIColor(named: "bgColor")?.cgColor
        notificationCount?.layer.borderWidth = 1
        notificationCount?.textColor = .white
        notificationCount?.font = UIFont(name: "MazzardM-SemiBold", size: 10)
        notificationCount?.backgroundColor = UIColor(hexString: "#C26356")
        notificationCount?.widthAnchor.constraint(greaterThanOrEqualToConstant: 14).isActive = true
        notificationCount?.heightAnchor.constraint(equalToConstant: 14).isActive = true
        notificationCount?.textAlignment = .center
        notificationCount?.layer.cornerRadius = 7
        notificationCount?.clipsToBounds = true
        notificationCount?.sizeToFit()
        
        let dd = UILabel()
        dv.addSubview(dd)
        dd.text = "Депозит:"
        dd.translatesAutoresizingMaskIntoConstraints = false
        dd.leftAnchor.constraint(equalTo: dv.leftAnchor, constant: 20).isActive = true
        dd.centerYAnchor.constraint(equalTo: dv.centerYAnchor).isActive = true
        dd.textColor =  UIColor(named: "dark")
        dd.font = UIFont(name: "MazzardM-SemiBold", size: 14)
        
        depositLabel = UILabel()
        dv.addSubview(depositLabel!)
        depositLabel?.text = "₴"+12000.formattedWithSeparatorDecimal
        depositLabel?.translatesAutoresizingMaskIntoConstraints = false
        depositLabel?.trailingAnchor.constraint(equalTo: notificationButton!.leadingAnchor, constant: -20).isActive = true
        depositLabel?.centerYAnchor.constraint(equalTo: dv.centerYAnchor).isActive = true
        depositLabel?.textColor =  UIColor(named: "dark")
        depositLabel?.font = UIFont(name: "MazzardM-SemiBold", size: 14)
        
        depositView = dv
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.notificationAction(_:)))
        notificationButton?.addGestureRecognizer(tap)
    }
    
    func addNavBar(_ title: String){
       
        let dv = UIView()
        self.view.addSubview(dv)
        dv.translatesAutoresizingMaskIntoConstraints = false
        dv.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -1).isActive = true
        dv.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 1).isActive = true
        dv.topAnchor.constraint(equalTo: self.depositView!.bottomAnchor,constant: -1).isActive = true
        dv.heightAnchor.constraint(equalToConstant: 42).isActive = true
        dv.layer.borderColor = UIColor(named: "dark")?.cgColor
        dv.layer.borderWidth = 1
        dv.backgroundColor = .clear
        
        
        var configuration = UIButton.Configuration.filled()
        configuration.title = ""
        configuration.image = UIImage(named: "arrow-left")
        configuration.titlePadding = 12
        configuration.imagePadding = 12
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 12, leading: 12, bottom: 12, trailing: 12)
        configuration.baseBackgroundColor = .clear
        
        backButton = UIButton(configuration: configuration, primaryAction: nil)
        dv.addSubview((backButton)!)
        backButton?.translatesAutoresizingMaskIntoConstraints = false
        backButton?.bottomAnchor.constraint(equalTo: dv.bottomAnchor).isActive = true
        backButton?.leadingAnchor.constraint(equalTo: dv.leadingAnchor, constant: 8).isActive = true
        backButton?.topAnchor.constraint(equalTo: dv.topAnchor).isActive = true
        backButton?.widthAnchor.constraint(equalToConstant: 40).isActive = true
        backButton?.backgroundColor = .clear
        
        
        
        
        let dd = UILabel()
        dv.addSubview(dd)
        dd.text = title
        dd.translatesAutoresizingMaskIntoConstraints = false
        dd.centerXAnchor.constraint(equalTo: dv.centerXAnchor).isActive = true
        dd.centerYAnchor.constraint(equalTo: dv.centerYAnchor).isActive = true
        dd.textColor =  UIColor(named: "dark")
        dd.font = UIFont(name: "MazzardM-SemiBold", size: 20)
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backAction(_:)))
        backButton?.addGestureRecognizer(tap)
    }
    
    @objc func notificationAction(_ sender: UITapGestureRecognizer? = nil) {
        if (!(self is NotificationViewController)) {
            let storyboard = UIStoryboard(name: "Tabbar", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func backAction(_ sender: UITapGestureRecognizer? = nil) {
       print("back click")
    }
    
    
    
}
