//
//  InviteViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 13.12.2021.
//

import Foundation
import UIKit

class InviteViewController: MyViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        
        addDeposit()
        addNavBar("Пригласить")
        self.phoneText.delegate = self
        let paragraphStyle = NSMutableParagraphStyle()

        paragraphStyle.lineHeightMultiple = 1.27
        infoLabel.attributedText = NSMutableAttributedString(string: "Введите контактный номер человека которого хотите пригласить, ему придёт уведомление с просьбой зарегистрироваться по ссылке.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.phoneAction(_:)))
        self.phoneView.addGestureRecognizer(tap)
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor(named: "dark")
        let doneButton = UIBarButtonItem(title: "Готово", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneClick(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()

        phoneText.inputAccessoryView = toolBar
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        let str = NSMutableAttributedString(string: "+380  ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
        
        
        str.append( convertPhone(phoneText.text ?? ""))
        phoneLabel.attributedText = str
    }
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "InviteAlertViewController") as? InviteAlertViewController {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1.27
            let i = Int.random(in: 0..<3)
            if i == 0 {
                vc.titleValue = "Приглашение отправлено"
                vc.value = NSMutableAttributedString(string: "Вы успешно отправили приглашение по номеру "+addSeparatePhone(phoneText.text ?? ""), attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
                vc.onDoneBlock = { result in
                    self.navigationController?.popViewController(animated: true)
                }
            } else if i == 1 {
                vc.titleValue = "Невозможно пригласить"
                vc.value = NSMutableAttributedString(string: "Невозможно отправить приглашение, номер ", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
                vc.value.append(NSMutableAttributedString(string: addSeparatePhone(phoneText.text ?? ""), attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor: UIColor(hexString: "C26356")]))
                vc.value.append(NSMutableAttributedString(string: " не действительный.\nВозможно номер был указан неправильно.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle]))
                vc.onDoneBlock = { result in
                    
                }
            } else {
                vc.titleValue = "Невозможно пригласить"
                vc.value = NSMutableAttributedString(string: "Пользователь с номером ", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
                vc.value.append(NSMutableAttributedString(string: addSeparatePhone(phoneText.text ?? ""), attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor: UIColor(hexString: "C26356")]))
                vc.value.append(NSMutableAttributedString(string: " уже зарегистрирован в приложении.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle]))
                vc.onDoneBlock = { result in
                    
                }
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func phoneAction(_ sender: UITapGestureRecognizer? = nil) {
        phoneText.becomeFirstResponder()
       
    }
    
    @objc func doneClick(sender: UIBarButtonItem) {
        phoneText.resignFirstResponder()
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
       if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
           self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height)
       }
    }

    @objc func keyboardWillDisappear(_ notification: NSNotification) {
       if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.view.transform = CGAffineTransform(translationX: 0, y: 0)
       }
   }
    @IBAction func phoneEdited(_ sender: Any) {
        let str = NSMutableAttributedString(string: "+380  ", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
        
        
        str.append(convertPhone(phoneText.text ?? ""))
        phoneLabel.attributedText = str
    }
    
    
    func convertPhone(_ phone: String) -> NSMutableAttributedString {
        var res = ""
        if phone.count > 0 {
            res += "( "+phone[0]
        } else {
            return NSMutableAttributedString(string: "( __ __ )  __ __  __ __   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)])
        }
        
        if phone.count > 1 {
            res += " "+phone[1] + " )  "
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: " __ )  __ __  __ __   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 2 {
            res += phone[2]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: "__ __  __ __   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 3 {
            res += " "+phone[3]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: " __  __ __   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 4 {
            res += "  "+phone[4]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: "  __ __   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 5 {
            res += " "+phone[5]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: " __   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 6 {
            res += "  "+phone[6]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: "   __ __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 7 {
            res += " "+phone[7]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: " __ __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        if phone.count > 8 {
            res += " "+phone[8]
        } else {
            let t = NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
            t.append(NSMutableAttributedString(string: " __", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.5)]))
            return t
        }
        
        return NSMutableAttributedString(string: res, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
    }
    
    func addSeparatePhone(_ phone: String) -> String {
        return "0" + phone[0] + phone[1] + "-" + phone[2] + phone[3] + "-" + phone[4] + phone[5] + "-" + phone[6] + phone[7] + phone[8]
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 9
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
