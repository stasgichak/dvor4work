//
//  CardsViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 14.12.2021.
//

import Foundation
import UIKit

class CardsViewController: MyViewController, UITableViewDelegate, UITableViewDataSource, UpdateTableView {
    func update() {
        tableView.reloadData()
    }
    override func viewDidLoad() {
        addDeposit()
        addNavBar("Способы оплаты")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        
        items = [
                    CardModel("ПриватБанк", number:"12**67", date: "01/24", isActive: true),
                    CardModel("Монобанк", number: "12**67", date: "02/25", isActive: false),
                    CardModel("УкрСиббанк", number: "12**67", date: "03/26", isActive: false),
                    CardModel("Райффайзен банк Аваль", number:" 34**89", date: "04/27", isActive: true),
                    CardModel("ПриватБанк", number: "34**89", date: "05/28", isActive: true),
                    CardModel("Креди Агриколь Банк", number: "12**67", date: "06/29", isActive: false),
                    CardModel("УкрСиббанк", number: "67**89", date: "07/30", isActive: true),
                    CardModel("Креди Агриколь Банк", number: "34**89", date: "08/31", isActive: false),
                    CardModel("Монобанк", number: "56**00", date: "09/32", isActive: false),
                    CardModel("Райффайзен банк Аваль", number: "56**00", date: "10/33", isActive: false),
                    CardModel("ПриватБанк", number: "56**00", date: "11/34", isActive: false),
                    CardModel("Креди Агриколь Банк", number: "56**00", date: "12/35", isActive: false)
                ]
    }
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    var items:[CardModel] = []
    @IBOutlet weak var tableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return items.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as? ProfileTitle else { return UITableViewCell() }
            cellBackView(cell)
            return cell
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add", for: indexPath)
            cellBackView(cell)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "card", for: indexPath) as? CardCell else { return UITableViewCell() }
            cell.titleLabel.text = items[indexPath.row].name
            cell.dateLabel.text = items[indexPath.row].date
            cell.numberLabel.text = items[indexPath.row].number
            if items[indexPath.row].isActive {
                cell.titleLabel.alpha = 1.0
                cell.numberLabel.alpha = 1.0
            } else {
                cell.titleLabel.alpha = 0.3
                cell.numberLabel.alpha = 0.3
            }
            cellBackView(cell)
            return cell
        }
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "CardViewController") as? CardViewController {
                vc.item = items[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.section == 2 {
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "AddCardViewController") as? AddCardViewController {
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}
