//
//  HistoryViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 05.12.2021.
//

import Foundation
import UIKit

class HistoryViewController: MyViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewDidLoad() {
        addDeposit()
        addNavBar("История бронирования")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        
        
        items = [(name: "Офис №2", date: "03.09 - 25.12.2021", price: "₴12 900.15"),
                 (name: "Соло Спот", date: "03.09 - 25.12.2021", price: "₴12 900.15"),
                 (name: "Квик Ворк", date: "03.09 - 25.12.2021", price: "₴12 900.15"),
                 (name: "Офис №2", date: "03.09 - 25.12.2021", price: "₴12 900.15")
        ]
        
        items.append(contentsOf: items)
        items.append(contentsOf: items)
        items.append(contentsOf: items)
        items.append(contentsOf: items)
    }
    
    @IBOutlet weak var tableView: UITableView!
    var items:[(name: String, date: String, price: String)?] = []
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? HistoryCell else { return UITableViewCell() }
        if let item = items[indexPath.row] {
            cell.nameLabel.text = item.name
            cell.dateLabel.text = item.date
            cell.priceLabel.text = item.price
        }
        cellBackView(cell)
        return cell
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
