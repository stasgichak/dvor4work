//
//  AddCardViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 16.12.2021.
//

import Foundation
import UIKit

class AddCardViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)

        self.numberView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.numberAction(_:))))
        self.nameView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.nameAction(_:))))
        self.cvView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cvAction(_:))))
        self.dateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dateAction(_:))))
        
        numberLabel.attributedText = convertNumber(numberField.text ?? "")
        cvLabel.attributedText = convertCV(cvField.text ?? "")
        dateLabel.attributedText = convertDate(dateField.text ?? "")
        nameLabel.attributedText = convertName(nameField.text ?? "")
        
        
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor(named: "dark")
        var doneButton = UIBarButtonItem(title: "Далее", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneNumberClick(sender:)))
        
        var spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()

        numberField.inputAccessoryView = toolBar
        
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor(named: "dark")
        doneButton = UIBarButtonItem(title: "Далее", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneDateClick(sender:)))
        
        spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()

        dateField.inputAccessoryView = toolBar
        
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor(named: "dark")
        doneButton = UIBarButtonItem(title: "Далее", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneCVClick(sender:)))
        
        spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()

        cvField.inputAccessoryView = toolBar
        
        
        
        numberField.delegate = self
        dateField.delegate = self
        cvField.delegate = self
        nameField.delegate = self
        
    }
    
    @objc func numberAction(_ sender: UITapGestureRecognizer? = nil) {
        numberField.becomeFirstResponder()
       
    }
    
    @objc func nameAction(_ sender: UITapGestureRecognizer? = nil) {
        nameField.becomeFirstResponder()
       
    }
    
    @objc func cvAction(_ sender: UITapGestureRecognizer? = nil) {
        cvField.becomeFirstResponder()
       
    }
    
    @objc func dateAction(_ sender: UITapGestureRecognizer? = nil) {
        dateField.becomeFirstResponder()
       
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {})
    }
    
    @IBOutlet weak var saveTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var saveButton: DVButton!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var cvView: UIView!
    @IBOutlet weak var cvLabel: UILabel!
    @IBOutlet weak var cvField: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var numberTitle: UILabel!
    @IBOutlet weak var cvTitle: UILabel!
    @IBOutlet weak var dateTitle: UILabel!
    @IBOutlet weak var nameTitle: UILabel!
    
    
    
    
    
    
    @IBAction func numberEdited(_ sender: Any) {
        if let s = sender as? UITextField {
            if s == numberField {
                numberLabel.attributedText = convertNumber(numberField.text ?? "")
            } else if s == cvField {
                cvLabel.attributedText = convertCV(cvField.text ?? "")
            }
            else if s == dateField {
               dateLabel.attributedText = convertDate(dateField.text ?? "")
            } else {
                nameLabel.attributedText = convertName(nameField.text ?? "")
            }
        }
        if validateData() {
            UIView.animate(withDuration: 0.2, animations: {
                self.saveButton.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.saveButton.alpha = 0.4
            })
        }
    }
    
    
    func convertNumber(_ number: String) -> NSMutableAttributedString {
        let res = NSMutableAttributedString(string: "", attributes: [:])
        
        for i in 0..<16 {
            if i<number.count {
                let t = NSMutableAttributedString(string: ((i % 4 == 0 && i != 0) ? " " : "") + number[i], attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
                res.append(t)
            } else {
                let t = NSMutableAttributedString(string: ((i % 4 == 0 && i != 0) ? " " : "") + "X", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.4)])
                res.append(t)
            }
            
        }
        
        return res
    }
    
    func convertCV(_ number: String) -> NSMutableAttributedString {
        let res = NSMutableAttributedString(string: "", attributes: [:])
        
        for i in 0..<3 {
            if i<number.count {
                let t = NSMutableAttributedString(string: number[i], attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
                res.append(t)
            } else {
                let t = NSMutableAttributedString(string: "X", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.4)])
                res.append(t)
            }
            
        }
        
        return res
    }
    
    
    func convertDate(_ number: String) -> NSMutableAttributedString {
        let res = NSMutableAttributedString(string: "", attributes: [:])
        
        for i in 0..<4 {
            if i<number.count {
                let t = NSMutableAttributedString(string: ((i % 2 == 0 && i != 0) ? " / " : "") + number[i], attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
                res.append(t)
            } else {
                let t = NSMutableAttributedString(string: ((i % 4 == 0 && i != 0) ? " / " : "") + "X", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.4)])
                res.append(t)
            }
            
        }
        
        return res
    }
    
    func convertName(_ number: String) -> NSMutableAttributedString {
        let res = NSMutableAttributedString(string: "", attributes: [:])
        
            if number.count>0 {
                let t = NSMutableAttributedString(string: number, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")])
                res.append(t)
            } else {
                let t = NSMutableAttributedString(string: "IVAN IVANOV", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "dark")?.withAlphaComponent(0.4)])
                res.append(t)
            }
            
        
        
        return res
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 1000
        if textField == numberField {
            maxLength = 16
        } else if textField == cvField {
            maxLength = 3
        } else if textField == dateField {
            maxLength = 4
        }
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    @IBAction func saveAction(_ sender: Any) {
        if saveButton.alpha == 1.0 {
            self.dismiss(animated: true, completion: {})
        } else {
            self.dismiss(animated: true, completion: {})
        }
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
       if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
           self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height)
       }
        if saveTopConstraint.constant == 98 {
            UIView.animate(withDuration: 0.2, animations: {
                self.saveTopConstraint.constant = 40
                self.view.layoutIfNeeded()
            })
        }
    }

    @objc func keyboardWillDisappear(_ notification: NSNotification) {
       if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.view.transform = CGAffineTransform(translationX: 0, y: 0)
       }
        if saveTopConstraint.constant == 40{
            UIView.animate(withDuration: 0.2, animations: {
                self.saveTopConstraint.constant = 98
                self.view.layoutIfNeeded()
            })
        }
   }
    
    @objc func doneNumberClick(sender: UIBarButtonItem) {
        dateField.becomeFirstResponder()
    }
    @objc func doneDateClick(sender: UIBarButtonItem) {
        cvField.becomeFirstResponder()
    }
    @objc func doneCVClick(sender: UIBarButtonItem) {
        nameField.becomeFirstResponder()
    }
    @objc func doneNameClick(sender: UIBarButtonItem) {
        nameField.resignFirstResponder()
    }
    @IBAction func numberStart(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.numberTitle.alpha = 1
        })
    }
    @IBAction func numberEnd(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.numberTitle.alpha = 0.4
        })
    }
    @IBAction func dateStart(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.dateTitle.alpha = 1
        })
    }
    @IBAction func dateEnd(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.dateTitle.alpha = 0.4
        })
    }
    @IBAction func cvStart(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.cvTitle.alpha = 1
        })
    }
    @IBAction func cvEnd(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.cvTitle.alpha = 0.4
        })
    }
    @IBAction func nameStart(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.nameTitle.alpha = 1
        })
    }
    @IBAction func nameEnd(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.nameTitle.alpha = 0.4
        })
    }
    
    func validateData() -> Bool {
        return (numberField.text?.count ?? 0) == 16 && (dateField.text?.count ?? 0) == 4 && (cvField.text?.count ?? 0) == 3 && (nameField.text?.count ?? 0) >= 2
    }
}
