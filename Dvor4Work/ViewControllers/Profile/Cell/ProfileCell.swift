//
//  ProfileCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 11.11.2021.
//

import Foundation
import UIKit
class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
}
