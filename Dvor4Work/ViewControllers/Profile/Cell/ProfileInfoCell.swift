//
//  ProfileInfoCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 11.11.2021.
//

import Foundation
import UIKit
class ProfileInfoCell: UITableViewCell {
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var familyLabel: UILabel!
    
    func setup(){
        
    }
    
}
