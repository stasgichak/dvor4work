//
//  ProfileTitle.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 15.12.2021.
//

import Foundation
import UIKit
class ProfileTitle: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
}
