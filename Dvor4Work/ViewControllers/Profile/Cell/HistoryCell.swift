//
//  HistoryCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 06.12.2021.
//

import Foundation
import UIKit
class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
}
