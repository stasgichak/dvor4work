//
//  CardCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 15.12.2021.
//

import Foundation
import UIKit
class CardCell: UITableViewCell {
    
    @IBOutlet weak var supportView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
}
