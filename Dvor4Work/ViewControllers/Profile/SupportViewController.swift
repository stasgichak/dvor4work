//
//  SupportViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 06.12.2021.
//

import Foundation
import UIKit

class SupportViewController: MyViewController {
    
    override func viewDidLoad() {
        addDeposit()
        addNavBar("Поддержка")
        
        let paragraphStyle = NSMutableParagraphStyle()

        paragraphStyle.lineHeightMultiple = 1.27
        infoLabel.attributedText = NSMutableAttributedString(string: "Поддержка проходит в чате Telegram. Перейдите в чат и задайте свой вопрос или опишите проблему с которой столкнулись при использовании нашего сервиса.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        
    }
    
    
    
    @IBOutlet weak var infoLabel: UILabel!
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        let botURL = URL.init(string: "tg://resolve?domain=Stanislav364")

        if UIApplication.shared.canOpenURL(botURL!) {
            UIApplication.shared.open(botURL!)
        } else {
          // Telegram is not installed.
        }
    }
}
