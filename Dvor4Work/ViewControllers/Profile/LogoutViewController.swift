//
//  LogoutViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 17.12.2021.
//

import Foundation
import UIKit

class LogoutViewController: UIViewController {
    
    override func viewDidLoad() {
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            
        })
        
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let t = self.onDoneBlock{
                t(true)
            }
        })
        
    }
    
    var onDoneBlock : ((Bool) -> Void)?
}
