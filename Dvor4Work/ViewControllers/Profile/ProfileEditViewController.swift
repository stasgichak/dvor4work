//
//  ProfileEditViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 15.11.2021.
//

import Foundation
import UIKit
class ProfileEditViewController: MyViewController, UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    override func viewDidLoad() {
        addDeposit()
        addNavBar("Редактирование")
        nameField.text = ProfileViewModel.shared.name
        familyField.text = ProfileViewModel.shared.family
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        familyField.delegate = self
        nameField.delegate = self
        var tap = UITapGestureRecognizer(target: self, action: #selector(self.avatarChange(_:)))
        if let i = ProfileViewModel.shared.avatar {
            self.avatarView.image = i
        } else {
            self.avatarView.image = UIImage(named: "avatar")
        }
        self.avatarView?.addGestureRecognizer(tap)
        tap = UITapGestureRecognizer(target: self, action: #selector(self.avatarChange(_:)))
        self.changeAvatarView.addGestureRecognizer(tap)
    }
    var imagePicker: UIImagePickerController!
    @IBOutlet weak var saveButton: DVButton!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var familyField: UITextField!
    @IBOutlet weak var changeAvatarView: UIView!
    @IBOutlet weak var avatarView: UIImageView!
    var image: UIImage?
    var delegate: UpdateTableView?
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {

       if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.saveButton.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height)
        
       }
    }

    @objc func keyboardWillDisappear(_ notification: NSNotification) {
       if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.saveButton.transform = CGAffineTransform(translationX: 0, y: 0)
       }
   }

    @IBAction func nameChanged(_ sender: Any) {
        //ProfileViewModel.shared.name = nameField.text ?? ""
    }
    @IBAction func fanilyChanged(_ sender: Any) {
        //ProfileViewModel.shared.family = familyField.text ?? ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == nameField {
            familyField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    @objc func avatarChange(_ sender: UITapGestureRecognizer? = nil) {
        showActionSheet()
    }
    
    func showActionSheet() {
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor(named: "lightgreen")
        let cameraActionButton = UIAlertAction(title: "Камера", style: .default) { _ in
            self.selectFileFrom(.camera)
        }
        actionSheet.addAction(cameraActionButton)
        let photoActionButton = UIAlertAction(title: "Галерея", style: .default) { _ in
            self.selectFileFrom(.photoLibrary)
        }
        actionSheet.addAction(photoActionButton)
        let removeActionButton = UIAlertAction(title: "Удалить фото", style: .destructive) { _ in
            self.selectFileFrom(.removeAvatar)
        }
        actionSheet.addAction(removeActionButton)
        let cancelActionButton = UIAlertAction(title: "Отмена", style: .cancel) { _ in
            //actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelActionButton)
        DispatchQueue.main.async {
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    var tempImage:UIImage?
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.editedImage] as? UIImage else {
            print("Image not found!")
            return
        }
        image = selectedImage
        self.avatarView.image = selectedImage
    }
    
    enum FileSource {
        case photoLibrary
        case camera
        case removeAvatar
    }

    func selectFileFrom(_ source: FileSource) {
        imagePicker =  UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        switch source {
            case .camera:
                imagePicker.sourceType = .camera
                present(imagePicker, animated: true, completion: nil)
            case .photoLibrary:
                imagePicker.sourceType = .photoLibrary
                present(imagePicker, animated: true, completion: nil)
            case .removeAvatar:
                image = nil
                self.avatarView.image = UIImage(named: "avatar")
                break
        }
    }
    @IBAction func saveAction(_ sender: Any) {
        ProfileViewModel.shared.name = nameField.text ?? ""
        ProfileViewModel.shared.family = familyField.text ?? ""
        ProfileViewModel.shared.avatar = image
        var vv = navigationController?.viewControllers
        delegate?.update()
        self.navigationController?.popViewController(animated: true)
    }
}
