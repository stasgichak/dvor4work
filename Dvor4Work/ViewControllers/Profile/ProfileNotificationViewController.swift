//
//  ProfileNotificationViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 01.12.2021.
//

import Foundation
import UIKit

class ProfileNotificationViewController: MyViewController {
    
    override func viewDidLoad() {
        addDeposit()
        addNavBar("Настройки уведомлений")
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.remindersAction(_:)))
        remindersView.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.balanceAction(_:)))
        balanceView.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.reservationAction(_:)))
        reservationView.addGestureRecognizer(tap3)
    }
    @IBOutlet weak var remindersView: UIView!
    @IBOutlet weak var remindersSlider: SliderView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceSlider: SliderView!
    @IBOutlet weak var reservationView: UIView!
    @IBOutlet weak var reservationSlider: SliderView!
    
    @objc func remindersAction(_ sender: UITapGestureRecognizer? = nil) {
        remindersSlider.isOn = !remindersSlider.isOn
    }
    
    @objc func balanceAction(_ sender: UITapGestureRecognizer? = nil) {
        balanceSlider.isOn = !balanceSlider.isOn
    }
    
    @objc func reservationAction(_ sender: UITapGestureRecognizer? = nil) {
        reservationSlider.isOn = !reservationSlider.isOn
    }
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
}
