//
//  ProfileViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 11.11.2021.
//

import Foundation
import UIKit
class ProfileViewController: MyViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UpdateTableView {
    func update() {
        tableView.reloadData()
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        addDeposit()
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "info", for: indexPath) as? ProfileInfoCell else { return UITableViewCell() }
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.avatarChange(_:)))
            if let i = ProfileViewModel.shared.avatar {
                cell.avatarView.image = i
            } else {
                cell.avatarView.image = UIImage(named: "avatar")
            }
            cell.nameLabel.text = ProfileViewModel.shared.name
            cell.familyLabel.text = ProfileViewModel.shared.family
            cell.avatarView?.addGestureRecognizer(tap)
            cellBackView(cell)
            return cell
        }
        else if indexPath.row == 6 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "logout", for: indexPath) as? ProfileLogoutCell else { return UITableViewCell() }
            cellBackView(cell)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ProfileCell else { return UITableViewCell() }
            if indexPath.row == 1 {
                cell.titleLabel.text = "Настройки уведомлений"
                cell.iconView.image = UIImage(named: "notificationIcon")
            } else if indexPath.row == 2 {
                cell.titleLabel.text = "История бронирования"
                cell.iconView.image = UIImage(named: "historyIcon")
            } else if indexPath.row == 3 {
                cell.titleLabel.text = "Способы оплаты"
                cell.iconView.image = UIImage(named: "cardsIcon")
            } else if indexPath.row == 4 {
                cell.titleLabel.text = "Пригласить"
                cell.iconView.image = UIImage(named: "inviteIcon")
            } else {
                cell.titleLabel.text = "Поддержка"
                cell.iconView.image = UIImage(named: "supportIcon")
            }
            cellBackView(cell)
            return cell
        }
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        if indexPath.row == 0 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "ProfileEditViewController") as? ProfileEditViewController {
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 1 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "ProfileNotificationViewController") as? ProfileNotificationViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 2 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as? HistoryViewController {
                //vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 3 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "CardsViewController") as? CardsViewController {
                //vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 4 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "InviteViewController") as? InviteViewController {
                //vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 5 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.row == 6 {
            if let vc = storyboard.instantiateViewController(withIdentifier: "LogoutViewController") as? LogoutViewController {
                vc.onDoneBlock = { result in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                self.present(vc, animated: true, completion: {})
            }
        } else {
            
        }
        
        
        
    }
    
    @objc func avatarChange(_ sender: UITapGestureRecognizer? = nil) {
        showActionSheet()
    }
    
    func showActionSheet() {
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor(named: "lightgreen")
        let cameraActionButton = UIAlertAction(title: "Камера", style: .default) { _ in
            self.selectFileFrom(.camera)
        }
        actionSheet.addAction(cameraActionButton)
        let photoActionButton = UIAlertAction(title: "Галерея", style: .default) { _ in
            self.selectFileFrom(.photoLibrary)
        }
        actionSheet.addAction(photoActionButton)
        let removeActionButton = UIAlertAction(title: "Удалить фото", style: .destructive) { _ in
            self.selectFileFrom(.removeAvatar)
        }
        actionSheet.addAction(removeActionButton)
        let cancelActionButton = UIAlertAction(title: "Отмена", style: .cancel) { _ in
            //actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelActionButton)
        DispatchQueue.main.async {
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    var tempImage:UIImage?
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.editedImage] as? UIImage else {
            print("Image not found!")
            return
        }
        ProfileViewModel.shared.avatar = selectedImage
        tableView.reloadData()
    }
    
    enum FileSource {
        case photoLibrary
        case camera
        case removeAvatar
    }

    func selectFileFrom(_ source: FileSource) {
        imagePicker =  UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        switch source {
            case .camera:
                imagePicker.sourceType = .camera
                present(imagePicker, animated: true, completion: nil)
            case .photoLibrary:
                imagePicker.sourceType = .photoLibrary
                present(imagePicker, animated: true, completion: nil)
            case .removeAvatar:
                ProfileViewModel.shared.avatar = nil
                tableView.reloadData()
                break
        }
    }
}

protocol UpdateTableView {
    func update()
}
