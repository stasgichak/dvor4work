//
//  InviteAlertViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 14.12.2021.
//

import Foundation
import UIKit

class InviteAlertViewController: UIViewController {
    
    override func viewDidLoad() {
        textView.attributedText = value
        titleView.text = titleValue
        textView.textAlignment = .center
    }
    var titleValue: String = ""
    var value: NSMutableAttributedString = NSMutableAttributedString()
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var textView: UILabel!
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let t = self.onDoneBlock{
                t(true)
            }
        })
        
    }
    
    var onDoneBlock : ((Bool) -> Void)?
}
