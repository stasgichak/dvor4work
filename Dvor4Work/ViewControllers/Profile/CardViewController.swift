//
//  CardViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 16.12.2021.
//

import Foundation
import UIKit

class CardViewController: MyViewController {
    
    override func viewDidLoad() {
        addDeposit()
        addNavBar("Настройки карты")
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.remindersAction(_:)))
        activeView.addGestureRecognizer(tap1)
        
        titleLabel.text = item?.name
        dateLabel.text = item?.date
        numberLabel.text = item?.number
        if item?.isActive ?? false {
            titleLabel.alpha = 1.0
            numberLabel.alpha = 1.0
        } else {
            titleLabel.alpha = 0.3
            numberLabel.alpha = 0.3
        }
        activeSlider.isOn = item?.isActive ?? false
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var activeView: UIView!
    @IBOutlet weak var activeSlider: SliderView!
    var item:CardModel?
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func remindersAction(_ sender: UITapGestureRecognizer? = nil) {
        activeSlider.isOn = !activeSlider.isOn
        if activeSlider.isOn {
            UIView.animate(withDuration: 0.2, animations: {
                self.titleLabel.alpha = 1.0
                self.numberLabel.alpha = 1.0
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.titleLabel.alpha = 0.3
                self.numberLabel.alpha = 0.3
            })
        }
    }
    @IBAction func deleteAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
