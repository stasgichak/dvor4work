//
//  NotificationCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 17.12.2021.
//

import Foundation
import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var nextButton: DVButton!
    
    
}
