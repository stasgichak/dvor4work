//
//  NotificationViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 17.12.2021.
//

import Foundation
import UIKit

class NotificationViewController: MyViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewDidLoad() {
        addDeposit()
        addNavBar("Уведомления")
        
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "balance", for: indexPath) as? NotificationCell else { return UITableViewCell() }
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1.27
            cell.valueLabel.attributedText = NSMutableAttributedString(string: "Средства на вашем депозите на исходе. Пополните свой баланс, чтобы продолжить пользование Зум-румом.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            cellBackView(cell)
            return cell
        }
        else if indexPath.row == 6 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "reserve", for: indexPath) as? NotificationCell else { return UITableViewCell() }
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1.27
            cell.valueLabel.attributedText = NSMutableAttributedString(string: "Средства на вашем депозите на исходе. Пополните свой баланс, чтобы продолжить пользование Зум-румом.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            cellBackView(cell)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "remember", for: indexPath) as? NotificationCell else { return UITableViewCell() }
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1.27
            let str = NSMutableAttributedString(string: "Вы забронировали вебинорку на 14:30. \n", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            str.append(NSMutableAttributedString(string: "Начало через 10 минут.", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor: UIColor(red: 0.761, green: 0.388, blue: 0.337, alpha: 1)]))
            cell.valueLabel.attributedText = str
            cellBackView(cell)
            return cell
        }
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
