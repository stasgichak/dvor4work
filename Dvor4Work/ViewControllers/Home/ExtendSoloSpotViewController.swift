//
//  ExtendSoloSpotViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 06.01.2022.
//

import Foundation
import UIKit
import EFCountingLabel
class ExtendSoloSpotViewController: UIViewController {
    
    override func viewDidLoad() {
        resultTitle.font = UIFont(name: "MazzardM-SemiBold", size: 20)
        resultValue.font = UIFont(name: "MazzardM-SemiBold", size: 25)
        
        self.tariff7View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tariff7Action(_:))))
        self.tariff1View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tariff1Action(_:))))
        self.tariff3View.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tariff3Action(_:))))
        
        resultValue.setUpdateBlock { value, label in
            label.text = "₴"+value.formattedWithSeparator
        }
        tariff7Image.image = UIImage(named: "check-on")
        resultValue.countFrom(1300, to: 1300, withDuration: 1)

    }
    @IBOutlet weak var tariff1View: UIView!
    @IBOutlet weak var tariff7Image: UIImageView!
    @IBOutlet weak var tariff7View: UIView!
    @IBOutlet weak var tariff1Image: UIImageView!
    @IBOutlet weak var tariff3View: UIView!
    @IBOutlet weak var tariff3Image: UIImageView!
    @IBOutlet weak var resultTitle: UILabel!
    @IBOutlet weak var resultValue: EFCountingLabel!
    @IBOutlet weak var dateFromLabel: UILabel!
    @IBOutlet weak var dateToLabel: UILabel!
    var tariff = 0
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tariff3Action(_ sender: UITapGestureRecognizer? = nil) {
        tariff = 2
        tariff3Image.image = UIImage(named: "check-on")
        resultValue.countFromCurrentValueTo(15600, withDuration: 1)
        tariff1Image.image = UIImage(named: "check-off")
        tariff7Image.image = UIImage(named: "check-off")
    }
    
    @objc func tariff1Action(_ sender: UITapGestureRecognizer? = nil) {
        tariff = 1
        tariff1Image.image = UIImage(named: "check-on")
        resultValue.countFromCurrentValueTo(5200, withDuration: 1)
        tariff7Image.image = UIImage(named: "check-off")
        tariff3Image.image = UIImage(named: "check-off")
    }
    
    @objc func tariff7Action(_ sender: UITapGestureRecognizer? = nil) {
        tariff = 0
        tariff7Image.image = UIImage(named: "check-on")
        resultValue.countFromCurrentValueTo(1300, withDuration: 1)
        tariff1Image.image = UIImage(named: "check-off")
        tariff3Image.image = UIImage(named: "check-off")
    }
    
}
