//
//  ReservAttrCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 02.01.2022.
//

import Foundation
import UIKit

class ReservAttrCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel?
    @IBOutlet weak var descLabel: UILabel?
}
