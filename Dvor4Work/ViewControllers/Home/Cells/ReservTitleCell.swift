//
//  ReservTitleCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 02.01.2022.
//

import Foundation
import UIKit

class ReservTitleCell: UITableViewCell {
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
