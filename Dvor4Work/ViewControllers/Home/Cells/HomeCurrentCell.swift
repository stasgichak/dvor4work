//
//  HomeCurrentCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 27.12.2021.
//

import Foundation
import UIKit
class HomeCurrentCell: UITableViewCell {
    
    @IBOutlet weak var guestLabel: UILabel!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    func setup(_ item: CurrentHomeModel) {
        self.guestLabel.attributedText = NSMutableAttributedString(string: getGuest(item.guest), attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue])
        
        switch item.type {
            
        case .office:
            statusLabel.text = "Офис-оунер:"
            logoImage.image = UIImage(named: "office-logo")
            let str = NSMutableAttributedString(string: "Офис №2\n", attributes: [:])
            str.append(NSMutableAttributedString(string: item.desc, attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            titleView.attributedText = str
        case .secondFloor:
            statusLabel.text = "Квик воркер:"
            logoImage.image = UIImage(named: "2nd-floor-logo")
            let str = NSMutableAttributedString(string: "Второй этаж\n", attributes: [:])
            str.append(NSMutableAttributedString(string:  "01", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            str.append(NSMutableAttributedString(string:  "ч.", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356"), NSMutableAttributedString.Key.font: UIFont(name: "MazzardH-SemiBold", size: 12)]))
            str.append(NSMutableAttributedString(string:  " 20", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            str.append(NSMutableAttributedString(string:  "мин.", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356"), NSMutableAttributedString.Key.font: UIFont(name: "MazzardH-SemiBold", size: 12)]))
            titleView.attributedText = str
        case .webinar:
            statusLabel.text = "Офис-оунер:"
            logoImage.image = UIImage(named: "webinar-logo")
            let str = NSMutableAttributedString(string: "Вебинарка №2\n", attributes: [:])
            str.append(NSMutableAttributedString(string: item.desc, attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            titleView.attributedText = str
        case .meetingRoom:
            statusLabel.text = "Квик воркер:"
            logoImage.image = UIImage(named: "meetingRoom-logo")
            let str = NSMutableAttributedString(string: "В переговорке\n", attributes: [:])
            str.append(NSMutableAttributedString(string: item.desc, attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            titleView.attributedText = str
        case .officeResident:
            statusLabel.text = "Резидент:"
            logoImage.image = UIImage(named: "office-logo")
            let str = NSMutableAttributedString(string: "Офис ", attributes: [:])
            str.append(NSMutableAttributedString(string: item.desc+"\n", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            titleView.attributedText = str
        case .soloSpot:
            statusLabel.text = "Резидент:"
            logoImage.image = UIImage(named: "soloSpot-logo")
            let str = NSMutableAttributedString(string: "Соло-спот\n", attributes: [:])
            str.append(NSMutableAttributedString(string: item.desc, attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#C26356")]))
            titleView.attributedText = str
        }
    }
    
    func getGuest(_ c: Int) -> String {
        if c % 10 == 1 {
            return String(c) + " гость"
        }
        if c % 10 == 2 || c % 10 == 3 || c % 10 == 4{
            return String(c) + " гостя"
        }
        return String(c) + " гостей"
    }
    
}
