//
//  HomeReservCell.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 31.12.2021.
//

import Foundation
import UIKit

class HomeReservCell: UITableViewCell {
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var hoursLabel: UILabel?
    @IBOutlet weak var minutesLabel: UILabel?
}
