//
//  ExtendMeetRoomViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 10.01.2022.
//

import Foundation
import UIKit
import EFCountingLabel
class ExtendMeetRoomViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, TimerViewDelegate {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if timeStartField.isEditing {
            return 93
        } else if timeEndField.isEditing {
            return 93 - currentStart
        } else {
            return 4
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if timeStartField.isEditing {
            let h = row / 4
            let m = (row % 4) * 15
            return ((h < 10) ? "0" : "") + String(h) + ":" + ((m < 10) ? "0" : "") + String(m)
        } else if timeEndField.isEditing {
            if 92 - currentStart == row{
                return "00:00"
            } else {
                let r = row + currentStart + 4
                let h = r / 4
                let m = (r % 4) * 15
                return ((h < 10) ? "0" : "") + String(h) + ":" + ((m < 10) ? "0" : "") + String(m)
            }
        } else {
            var dayComponent    = DateComponents()
            dayComponent.day    = row // For removing one day (yesterday): -1
            let theCalendar     = Calendar.current
            if let nextDate        = theCalendar.date(byAdding: dayComponent, to: Date()) {
            
                let formatter = DateFormatter()
                formatter.locale = Locale.current
                formatter.dateFormat = "dd MMMM, yyyy"
                return (formatter.string(from: nextDate))
            }
            return ""
        }
    }
    
    
    override func viewDidLoad() {
        resultTitle.font = UIFont(name: "MazzardM-SemiBold", size: 20)
        resultValue.font = UIFont(name: "MazzardM-SemiBold", size: 25)
        
        //self.timeStartField.setInputViewTimePicker(target: self, selector: #selector(tapDone))
        
        setInputViewTimePicker(timeStartField)
        setInputViewTimePicker(timeEndField)
        setInputViewTimePicker(dateToField)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        self.dateToView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dateAction(_:))))
        self.timeStartView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.timeStartAction(_:))))
        self.timeEndView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.timeEndAction(_:))))
        
        resultValue.countFrom(300, to: 300)
        
        timerView.delegate = self
        
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "dd MMMM, yyyy"
        dateToLabel.text = (formatter.string(from: Date()))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        timerView.setDate(Date())
    }
    
    func setInputViewTimePicker(_ textField: UITextField){
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.delegate = self
        datePicker.dataSource = self
        datePicker.tintColor = UIColor(named: "dark")
        datePicker.backgroundColor = UIColor(named: "bgColor")
        textField.inputView = datePicker
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
        toolBar.tintColor = UIColor(named: "dark")
        toolBar.backgroundColor = UIColor(named: "bgColor")
        toolBar.barTintColor = UIColor(named: "bgColor")
        let flexible1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let flexible2 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var name = ""
        if timeStartField == textField{
            name = "Начало:"
        } else  if timeEndField == textField {
            name = "Окончание:"
        } else if dateToField == dateToField {
            name = "Дата брони:"
        }
        let text = UIBarButtonItem(title: name, style: .done, target: self, action: nil)
        
        let cancel = UIBarButtonItem(title: "Отмена", style: .done, target: target, action: #selector(tapC))
        let barButton = UIBarButtonItem(title: "Готово", style: .done, target: target, action: #selector(tapDone))
        toolBar.setItems([cancel, flexible1, text, flexible2, barButton], animated: false)
        textField.inputAccessoryView = toolBar
    }
    
    @objc func tapC() {
        if timeStartField.isEditing {
            currentStart = tempStrart
            updateStartTime()
        } else if timeEndField.isEditing {
            currentEnd = tempEnd
            updateEndTime()
        } else {
            currentDate = tempDate
            updateDate()
        }
        
        timeStartField.resignFirstResponder()
        timeEndField.resignFirstResponder()
        dateToField.resignFirstResponder()
    }
    
    @objc func tapDone() {
        
        timeStartField.resignFirstResponder()
        timeEndField.resignFirstResponder()
        dateToField.resignFirstResponder()
    }
    
   
    @IBOutlet weak var doneButton: DVButton!
    @IBOutlet weak var timerView: TimerView!
    @IBOutlet weak var dateToField: UITextField!
    @IBOutlet weak var dateToView: UIView!
    @IBOutlet weak var resultTitle: UILabel!
    @IBOutlet weak var resultValue: EFCountingLabel!
    @IBOutlet weak var dateFromLabel: UILabel!
    @IBOutlet weak var dateToLabel: UILabel!
    @IBOutlet weak var timeStartField: UITextField!
    @IBOutlet weak var timeEndField: UITextField!
    @IBOutlet weak var timeStartLabel: UILabel!
    @IBOutlet weak var timeEndLabel: UILabel!
    @IBOutlet weak var timeEndView: UIView!
    @IBOutlet weak var timeStartView: UIView!
    @IBOutlet weak var countTimeLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    var currentStart = 0
    var currentEnd = 0
    var tempStrart = 0
    var tempEnd = 0
    var currentDate = 0
    var tempDate = 0
    var currentPrice = 300
    
    @IBAction func doneAction(_ sender: Any) {
        if doneButton.alpha == 1 {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if timeStartField.isEditing {
            currentStart = row
            updateStartTime()
        } else if timeEndField.isEditing {
            currentEnd = row
            
            updateEndTime()
        } else if dateToField.isEditing{
            currentDate = row
            updateDate()
        } else{
            
        }
        
    }
    @objc func keyboardWillAppear(_ notification: NSNotification) {
        if !dateToField.isEditing {
           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
               self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardSize.height)
           }
        }
    }

    @objc func keyboardWillDisappear(_ notification: NSNotification) {
       if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.view.transform = CGAffineTransform(translationX: 0, y: 0)
       }
   }
    
    func updateStartTime() {
        let h = currentStart / 4
        let m = (currentStart % 4) * 15
        timeStartLabel.text =  ((h < 10) ? "0" : "") + String(h) + ":" + ((m < 10) ? "0" : "") + String(m)
        
       
        updateCountTime()
    }
    
    func updateEndTime() {
        let r = currentEnd + currentStart + 4
        let h = r / 4
        let m = (r % 4) * 15
        if h != 24 {
        timeEndLabel.text = ((h < 10) ? "0" : "") + String(h) + ":" + ((m < 10) ? "0" : "") + String(m)
        } else {
            timeEndLabel.text = "00:00"
        }
        updateCountTime()
    }
    
    func updateDate() {
        var dayComponent    = DateComponents()
        dayComponent.day    = currentDate // For removing one day (yesterday): -1
        let theCalendar     = Calendar.current
        if let nextDate        = theCalendar.date(byAdding: dayComponent, to: Date()) {
        
            let formatter = DateFormatter()
            formatter.locale = Locale.current
            formatter.dateFormat = "dd MMMM, yyyy"
            timerView.setDate(nextDate)
            dateToLabel.text = (formatter.string(from: nextDate))
        }
    }
    
    @IBAction func startTimeEndEditing(_ sender: Any) {
        currentEnd = currentEnd  - currentStart + tempStrart
        tempStrart = currentStart
        
        validateEndTime()
        if currentEnd < 0 {
            currentEnd = tempEnd
            return
        }
        updateCountTime()
        timerView.setPeriod(currentStart, end: currentEnd+currentStart+3)
    }
    
    func validateEndTime(){
        if let t2 = timeEndLabel.text {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            var d2 = (formatter.date(from: t2)) ?? Date()
            if t2 == "00:00" {
                var dayComponent    = DateComponents()
                dayComponent.day    = 1 // For removing one day (yesterday): -1
                let theCalendar     = Calendar.current
                if let nextDate        = theCalendar.date(byAdding: dayComponent, to: d2) {
                    d2 = nextDate
                }
            }
            let d1 = (formatter.date(from: timeStartLabel.text ?? "00:00")) ?? Date()
            let diffComponents = Calendar.current.dateComponents([.hour], from: d1, to: d2)
            if let h = diffComponents.hour  {
                if h < 1 {
                    currentEnd = 0
                    updateEndTime()
                }
            }
        }
    }
    @IBAction func endTimeEndEditing(_ sender: Any) {
        tempEnd = currentEnd
        
        timerView.setPeriod(currentStart, end: currentEnd+currentStart+3)
    }
    
    @IBAction func dateEndEditing(_ sender: Any) {
        tempDate = currentDate
    }
    @objc func dateAction(_ sender: UITapGestureRecognizer? = nil) {
        dateToField.becomeFirstResponder()
    }
    @IBAction func dateToChangeAction(_ sender: Any) {
        dateToLabel.text = dateToField.text
    }
    @IBAction func dateToStartEditing(_ sender: Any) {
        if let d = dateToField.inputView as? UIPickerView {
            d.selectRow(currentDate, inComponent: 0, animated: false)
        }
    }
    
    @objc func timeStartAction(_ sender: UITapGestureRecognizer? = nil) {
        
        timeStartField.becomeFirstResponder()
        if let d = timeStartField.inputView as? UIPickerView {
            d.selectRow(currentStart, inComponent: 0, animated: false)
        }
    }
    @IBAction func timeStartChangeAction(_ sender: Any) {
        timeStartLabel.text = timeStartField.text
    }
    
    @objc func timeEndAction(_ sender: UITapGestureRecognizer? = nil) {
        timeEndField.becomeFirstResponder()
        if let d = timeEndField.inputView as? UIPickerView {
            d.selectRow(currentEnd, inComponent: 0, animated: false)
        }
    }
    @IBAction func timeEndChangeAction(_ sender: Any) {
        timeEndLabel.text = timeEndField.text
    }
    
    @IBAction func minusAction(_ sender: Any) {
        if minusButton.alpha == 1 {
            currentEnd = currentEnd - 1
            updateEndTime()
            timerView.setPeriod(currentStart, end: currentEnd+currentStart+3)
        }
        
    }
    @IBAction func plusAction(_ sender: Any) {
        if plusButton.alpha == 1 {
            currentEnd = currentEnd + 1
            updateEndTime()
            timerView.setPeriod(currentStart, end: currentEnd+currentStart+3)
        }
    }
    
    func updateCountTime(){
        
        let c = currentEnd + 4
        if c <= 4 {
            UIView.animate(withDuration: 0.2, animations: {
                self.minusButton.alpha = 0.5
            })
            
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.minusButton.alpha = 1
            })
        }
        
         if currentStart + c >= 96 {
            UIView.animate(withDuration: 0.2, animations: {
                self.plusButton.alpha = 0.5
            })
         } else {
             UIView.animate(withDuration: 0.2, animations: {
                 self.plusButton.alpha = 1
             })
         }
        let h = c / 4
        let m = (c % 4) * 15
        countTimeLabel.text =  ((h < 10) ? "0" : "") + String(h) + ":" + ((m < 10) ? "0" : "") + String(m)
        //print(c*75)
        resultValue.countFromCurrentValueTo((CGFloat(c*75)), withDuration: 0.2)
        //resultValue.countFrom(CGFloat(currentPrice), to: (CGFloat(c*75)), withDuration: 0.2)
        currentPrice = c*75
    }
    
    func startTimerViewChanged(_ start: Int) {
        currentEnd = currentEnd  - start + currentStart
        self.currentStart = start
        updateStartTime()
        validateEndTime()
        
        //updateCountTime()
        
    }
    
    func endTimerViewChanged(_ end: Int) {
        self.currentEnd = end - 4 - self.currentStart
        updateEndTime()
        //updateCountTime()
    }
    
    func changeStatePeriod(_ state: Bool) {
        if state {
            UIView.animate(withDuration: 0.2, animations: {
                self.doneButton.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.doneButton.alpha = 0.5
            })
        }
    }
    
    
}
