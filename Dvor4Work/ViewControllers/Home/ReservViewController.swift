//
//  ReservViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 01.01.2022.
//

import Foundation
import UIKit

class ReservViewController: MyViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewDidLoad() {
        
        addDeposit()
        addNavBar("Мои брони")
        
        if let v = item {
            vm = ReservViewModel(v)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)

        self.cancelView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.cancelAction(_:))))
        
        let gradientMaskLayer = CAGradientLayer()
        gradientMaskLayer.frame = gradientView.bounds
        gradientMaskLayer.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
        gradientMaskLayer.locations = [0, 1]
        gradientView.layer.mask = gradientMaskLayer
    }
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var vm: ReservViewModel?
    var item: ReserveModel?
    
    override func backAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func extendAction(_ sender: Any) {
        if item?.type == .office {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "ExtendOfficeViewController") as? ExtendOfficeViewController {
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        if item?.type == .soloSpot {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "ExtendSoloSpotViewController") as? ExtendSoloSpotViewController {
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        if item?.type == .meetingRoom {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "ExtendMeetRoomViewController") as? ExtendMeetRoomViewController {
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func cancelAction(_ sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return vm?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm?.items[section].value.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let i = vm?.items[indexPath.section]
        if i?.cellType == .title {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as? ReservTitleCell else { return UITableViewCell() }
            if let name = i?.value[indexPath.row].value as? (title: String, image: UIImage) {
                cell.titleLabel.text = name.title
                cell.logoView.image = name.image
            }
            cellBackView(cell)
            return cell
        } else
        
        if i?.cellType == .attr {
            let j = i?.value[indexPath.row]
            
            if j?.cellType == .time {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "attrDesc", for: indexPath) as? ReservAttrCell else { return UITableViewCell() }
                cell.titleLabel.text = j?.cellType.rawValue
                if let name = i?.value[indexPath.row].value as? (time: String, count: String) {
                    cell.valueLabel?.text = name.time
                    cell.descLabel?.text = name.count
                }
                cellBackView(cell)
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "attr", for: indexPath) as? ReservAttrCell else { return UITableViewCell() }
                cell.titleLabel.text = j?.cellType.rawValue
                cell.valueLabel?.text = j?.value as? String
                cellBackView(cell)
                return cell
            }
        } else
        if i?.cellType == .resident {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "resident", for: indexPath) as? ReservAttrCell else { return UITableViewCell() }
            if let name = i?.value[indexPath.row].value as? (name: String, phone: String) {
                cell.titleLabel.text = name.name
                cell.valueLabel?.text = name.phone
            }
            cellBackView(cell)
            return cell
        } else
        
        if i?.cellType == .addResident {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "add", for: indexPath) as? ReservAttrCell else { return UITableViewCell() }
            let j = i?.value[indexPath.row]
            cell.titleLabel.text = j?.cellType.rawValue
            cellBackView(cell)
            return cell
        } else
        
        if i?.cellType == .changePlace {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "change", for: indexPath) as? ReservAttrCell else { return UITableViewCell() }
            let j = i?.value[indexPath.row]
            cell.titleLabel.text = j?.cellType.rawValue
            cellBackView(cell)
            return cell
        } else {
            return UITableViewCell()
        }
       
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
