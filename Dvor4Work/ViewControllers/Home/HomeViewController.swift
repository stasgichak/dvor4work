//
//  HomeViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 27.12.2021.
//

import Foundation
import UIKit
class HomeViewController: MyViewController, UITableViewDelegate, UITableViewDataSource, UpdateTableView {
    func update() {
        tableView.reloadData()
    }
    var current:[CurrentHomeModel] = []
    var items: [ReserveModel] = []
    @IBOutlet weak var tableView: UITableView!
    

    
    override func viewDidLoad() {
        
        current = [
            CurrentHomeModel( .office, desc: "2 резидента"),
            //CurrentHomeModel( .secondFloor, guest: 3,desc: "01ч. 20мин."),
            //CurrentHomeModel( .webinar, desc: "до 18:30"),
            //CurrentHomeModel( .meetingRoom, guest: 1, desc: "до 16:45"),
            //CurrentHomeModel( .officeResident, guest: 8,desc: "№2"),
            //CurrentHomeModel( .soloSpot, guest: 101, desc: "№2 Кабинет"),
        ]
        
        items = [
            ReserveModel("Смарт офис", floor: "1 этаж", type: .office, toDate: "01.10.2021"),
            ReserveModel("Соло Спот", floor: "1 этаж", type: .soloSpot, toDate: "01.10.2021"),
            ReserveModel("Квик Ворк", minutes: "03", seconds: "25"),
            ReserveModel("Переговорка", floor: "2 этаж", type: .meetingRoom, toDate: "01.10.2021", fromTime: "12:30", toTime: "14:30"),
            ReserveModel("Вебинарка", floor: "2 этаж", type: .webinar, toDate: "01.10.2021", fromTime: "12:30", toTime: "14:30")
        ]
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        addDeposit()
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return current.count
        }
        if section == 1 {
            return 1
        }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "current", for: indexPath) as? HomeCurrentCell else { return UITableViewCell() }
            cell.setup(current[indexPath.row])
            cellBackView(cell)
            return cell
        }
        if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as? ProfileTitle else { return UITableViewCell() }
            cell.titleLabel.attributedText = NSMutableAttributedString(string: "МОИ БРОНИ", attributes: [NSAttributedString.Key.kern: 0.6])
            cellBackView(cell)
            return cell
        }
        else {
            let i = items[indexPath.row]
            if i.type == .soloSpot || i.type == .office {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "reserv", for: indexPath) as? HomeReservCell else { return UITableViewCell() }
                cell.dateLabel.text = items[indexPath.row].toDate
                var str = NSMutableAttributedString(string: items[indexPath.row].name+"  ", attributes: [:])
                str.append(NSMutableAttributedString(string: (items[indexPath.row].floor ?? ""), attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "C26356")]))
                cell.titleLabel.attributedText = str
                cellBackView(cell)
                return cell
            } else if i.type == .secondFloor {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "reservQuick", for: indexPath) as? HomeReservCell else { return UITableViewCell() }
                var str = NSMutableAttributedString(string: items[indexPath.row].name+"  ", attributes: [:])
                str.append(NSMutableAttributedString(string: (items[indexPath.row].floor ?? ""), attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "C26356")]))
                cell.titleLabel.attributedText = str
                cell.hoursLabel?.text = i.minutes
                cell.minutesLabel?.text = i.seconds
                cellBackView(cell)
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "reservTime", for: indexPath) as? HomeReservCell else { return UITableViewCell() }
                cell.dateLabel.text = items[indexPath.row].toDate
                var str = NSMutableAttributedString(string: items[indexPath.row].name+"  ", attributes: [:])
                str.append(NSMutableAttributedString(string: (items[indexPath.row].floor ?? ""), attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "C26356")]))
                cell.titleLabel.attributedText = str
                cell.timeLabel?.text = "с " + (i.fromTime ?? "") + " до " + (i.toTime ?? "")
                cellBackView(cell)
                return cell
            }
        }
    }
    
    func cellBackView(_ cell:UITableViewCell?){
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell!.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let i = items[indexPath.row]
            if i.type != .secondFloor {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier: "ReservViewController") as? ReservViewController {
                    vc.item = i
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
}

