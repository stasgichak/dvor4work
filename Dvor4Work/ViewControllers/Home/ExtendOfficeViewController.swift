//
//  ExtendOfficeViewController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 03.01.2022.
//

import Foundation
import UIKit
import EFCountingLabel
class ExtendOfficeViewController: UIViewController {
    
    override func viewDidLoad() {
        resultTitle.font = UIFont(name: "MazzardM-SemiBold", size: 20)
        resultValue.font = UIFont(name: "MazzardM-SemiBold", size: 25)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tariff3Action(_:)))
        self.tariff3View.addGestureRecognizer(tap)
        
        resultValue.setUpdateBlock { value, label in
            label.text = "₴"+value.formattedWithSeparator
        }
    }
    @IBOutlet weak var tariff3View: UIView!
    @IBOutlet weak var tariff3Image: UIImageView!
    @IBOutlet weak var resultTitle: UILabel!
    @IBOutlet weak var resultValue: EFCountingLabel!
    @IBOutlet weak var dateFromLabel: UILabel!
    @IBOutlet weak var dateToLabel: UILabel!
    var tariff3 = false
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tariff3Action(_ sender: UITapGestureRecognizer? = nil) {
        tariff3 = !tariff3
        if tariff3 {
            tariff3Image.image = UIImage(named: "check-on")
            resultValue.countFrom(14000, to: 42000, withDuration: 1)
        } else {
            tariff3Image.image = UIImage(named: "check-off")
            resultValue.countFrom(42000, to: 14000, withDuration: 1)
        }
    }
    
}
