//
//  MainTabbarController.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 09.11.2021.
//

import Foundation
import UIKit
class MainTabbarController: UITabBarController {
    
    override func viewDidLoad() {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "MazzardM-Regular", size: 14)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "MazzardM-Regular", size: 14)!], for: .selected)
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        let window = windowScene?.windows.first
        
        let line = UIView()
        self.view.addSubview(line)
        line.translatesAutoresizingMaskIntoConstraints = false
        line.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        line.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        line.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,constant: -self.tabBar.height+1-( window?.safeAreaInsets.bottom ?? 0)).isActive = true
        line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        line.backgroundColor = UIColor(named: "dark")
        self.navigationItem.hidesBackButton = true
        
        let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithDefaultBackground()
            tabBarAppearance.backgroundColor = UIColor(named: "bgColor")
            UITabBar.appearance().standardAppearance = tabBarAppearance

            if #available(iOS 15.0, *) {
                UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
            }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}


