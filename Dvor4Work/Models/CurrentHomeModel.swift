//
//  CurrentHomeModel.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 27.12.2021.
//

import Foundation

class CurrentHomeModel {
    
    init(_ type: CurrentHomeType, guest: Int? = nil, time: Date? = nil, desc: String? = nil){
        self.type = type
        self.guest = guest ?? 0
        self.time = time ?? Date()
        self.desc = desc ?? ""
    }
    
    var guest = 0
    var type:CurrentHomeType = .office
    var time = Date()
    var desc = ""
}

enum CurrentHomeType {
    case office
    case secondFloor
    case webinar
    case meetingRoom
    case officeResident
    case soloSpot
}
