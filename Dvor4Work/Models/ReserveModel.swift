//
//  ReserveModel.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 27.12.2021.
//

import Foundation

class ReserveModel {
    
    var minutes: String?
    var seconds: String?
    var fromDate: String = ""
    var toDate: String = ""
    var fromTime: String?
    var toTime: String?
    var hoursCount: String?
    var name: String = ""
    var tariff: String = ""
    var guest: [(name: String, desc: String)] = []
    var floor: String?
    var type: CurrentHomeType = .secondFloor
    
    init(_ name: String, minutes: String, seconds: String){
        self.name = name
        self.minutes = minutes
        self.seconds = seconds
        self.floor = "2 этаж"
        self.type = .secondFloor
    }
    
    init(_ name: String, floor: String, type:CurrentHomeType, toDate: String, fromTime: String? = nil, toTime: String? = nil){
        self.name = name
        self.toDate = toDate
        self.fromTime = fromTime
        self.toTime = toTime
        self.type = type
        self.floor = floor
    }
    
    
}
