//
//  CardModel.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 15.12.2021.
//

import Foundation
import UIKit

class CardModel {
    
    var name = ""
    var number = ""
    var date = ""
    var isActive = false
    
    init(_ name: String, number: String, date: String, isActive: Bool) {
        self.name = name
        self.number = number
        self.date = date
        self.isActive = isActive
    }
    
}
