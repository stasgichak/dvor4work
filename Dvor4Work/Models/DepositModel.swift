//
//  DepositModel.swift
//  Dvor4Work
//
//  Created by Stanislav Hychak on 26.12.2021.
//

import Foundation
import UIKit

class DepositModel {
    
    var name = ""
    var number = ""
    var date = ""
    var time = ""
    var price:Double = 0
    
    init(_ name: String, number: String, date: String, time: String, price: Double) {
        self.name = name
        self.number = number
        self.date = date
        self.time = time
        self.price = price
    }
    
}
